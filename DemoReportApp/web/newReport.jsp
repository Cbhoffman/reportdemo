<%-- 
    Document   : test
    Created on : 26-May-2017, 14:52:42
    Author     : Caleb Hoffman
--%>

<%@page import="SystemInteraction.ReportTypes"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--Landing page for users wishing to submit a report.-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>Submitting Report</title>
     
    </head>
    <body>
        <section class='hero is-fullheight is-light is-bold'>
            <br>
        <div class="container">
             <div class="columns is-vcentered"> 
                 <div class="column is-8 is-offset-2">
                     
        <div class="level">
            <h1 class="title">Submit your report here.</h1>
            <form action='Logout'>   
            <input type='submit' class='button is-light' value='Logout' />
            </form>
        </div>
    
        <form name= 'report' action='SubmitReport' method='post' enctype="multipart/form-data" onSubmit="return validate()" >
           <div class='box'>
            <div class='field'>
                <div class='control'>
            <label class="label">Please select the area that applies the most to your report:</label>
            <%
              List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
              for(ReportTypes.Type type : types)
              { 
                  String value = type.id;
                  String insert = type.name;

                  //Displays the different report types (from reporttypes class) within the form.
                  out.println("<input type='radio' name='reportType' value='"+value+"' /> "+insert+"<br />");
              }
            %>
            
                </div>
            </div>
        <br>    
            
         <!--
            First Name: <input type="text" name="fName" placeholder="First name" /><br />
            Last Name: <input type="text" name="lName" placeholder="Last name" /><br /> 
            Contact Number: <input type="text" name="contact" /><br /> -->
    <!--        <p> Email: <input type="text" name="email" /> </p>   -->
            
        <div class='field'>
            <label class="label"> Level of Urgency: </label>
            <div class='control'>
                <div class='select'>
                <select name="urgency">
          <!--          <option value="default" selected="selected"></option> -->
                    <option value="low" selected="selected"> Low</option>
                    <option value="medium"> Medium</option>
                    <option value="high"> High</option>
                </select>
                </div>
            </div>    
        </div>
          
            <br>
                        
        <div class='field'>
            <label class="label"> Insert Photo: </label>
            <div class='control'>
            <!-- Insert Photo: <input type="file" name="file" size="50" capture="camera" accept="image/*"/><br /><br /> -->
            <input type="file" name="file" size="40" accept="image/*">  
            </div>
        </div>
            
            <br>
            
        <div class='field'>
            <label class="label"> Provide Geolocation Information </label>
            <div class='control'>
            <input type="checkbox" id="location" name="location" value="geolocation" onclick="getLocation()">  (Only if you are at the location where your issue has occured.)
            </div>
        </div>   
            
            <br>
            
        <div class='field'>
            <label class="label"> Report Details: </label>
            <div class='control'>
            <textarea class="textarea" cols="30" rows="4" name="text"></textarea> 
            </div>
        </div>
            <br>
            
            <div class="level">
   
            <input type="submit" class="button is-primary" value="Submit Your Report"/>    
    
            <a class='button is-primary is-outlined' href='UserView'>Back to your Profile Page</a>    
                             

            
            </div>
           </div>
        </form>


            
            <p id="demo">

            <script type="text/javascript" src="reportjs.js"></script>  
                 </div>
            </div>
        </div>
            <br>
        </section>
    </body>
</html>