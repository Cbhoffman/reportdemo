<%-- 
    Document   : support
    Created on : 16-May-2017, 14:29:47
    Author     : Caleb Hoffman
--%>

<%@page import="SystemInteraction.ReportTypes"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--Support workers arrive here after login process is successful.-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="reportjs.js" type="text/javascript"></script>
        <title>Support Page</title>
    </head>
    <body>
        <h1>View Submitted Reports</h1>
        <form name= "support" action="NewSupportView?page=1" method="get">
            <h2>Please select the relevant type.</h2>

<%
                List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                for(ReportTypes.Type type : types)
                { 
                    String value = type.id;
                    String insert = type.name;
                                 
                    out.println("<input type='radio' name='searchType' value='"+value+"' />"+insert+"<br />");
                }
%>
                <br />
                <br />
                
                <br />
                
                <input type="submit" onclick= "testing()" value="Submit" />
            </form>
       
    </body>
</html>

