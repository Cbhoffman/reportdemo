<%-- 
    Document   : addComment
    Created on : 21-Feb-2017, 10:59:00
    Author     : Caleb Hoffman
--%>
<!-- NOT NEEDED... USED FOR TESTING DESIGN OF SERVLET-->
<%@page import="java.util.List"%>
<%//@page import="uk.ac.uea.cmp.javaweb.Comments.Comment"%>
<%//@page import="uk.ac.uea.cmp.javaweb.Comments"%>
<%//@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
-->
    <%
        //Comments c = new Comments();
        
        // Is there a new comment to add at this point? If so add it now.
        // n.b. we could also do this on a seperate page and redirect back
        // such as addComment.jsp but here for simplicity EVEN IF NOT GOOD
        // DESIGN PRACTICE!
        
        //String name = request.getParameter("name");
        //String text = request.getParameter("text");
        
        //if (name != null && text != null)
        //{
        //    c.Add(name, text);
        //}
        
        // Now read and print the comments
        //List<Comment> comments = c.Get();
        //for(Comment comment: comments)          
        //{
        //    out.print("<b>By: "+comment.name+", at "+comment.posted+"</b><br /><br />");
        //    out.print(comment.text);
        //    out.print("<br /><br /><hr />");
        //}

/*
if(request.getParameter("btnSubmit")!=null) //btnSubmit is the name of your button, not id of that button.
{
     comments.clear();
}
*/
String email = "test@test.com";
String text = "Some random report being submitted into the system.";
String reportType = "security";
String urgency = "low";
String geolocation = "coordinates";

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Submission Confirmation</title>");   
            out.println("<link rel='stylesheet' href='bulma.css'>");
            out.println("</head>");
            out.println("<body>");  
            out.println("<div class='container content'>");
         //   out.println("<div class='columns is-centered'> ");
            out.println("<div class='column is-two-thirds'>");
         //   out.println("<section class='hero is-primary is-medium'>");
         //   out.println("<div class='container'>");
         //   out.println("<div class='hero-body'>");
            out.println("<h1>Thank you for your report.</h1>");
            out.println("<h2>You just submitted the following data:</h2>");  
            out.println("<p>Email: "+email+"</p>");
            out.println("<p>Details: "+text+"</p>");
            out.println("<p>Report Type: "+reportType+"</p>");
            out.println("<p>Urgency: "+urgency+"</p>");
            // Future work: possible inclusion of a map that displays the given geolocation?
            out.println("<p>Location Coordinates: "+geolocation+"</p>");
            
            out.println("<h3>You will receive an email with your report id number that you can use to track the status of your report.</h3>");  
            
            out.println("<p id='demo'></p>");
            
            out.println("<a href='home.jsp'>Back to the Home Page</a><br /><br />");
        //    out.println("</div>");
        //    out.println("</div>");
        //    out.println("</div>");
            out.println("</div>");
        //    out.println("</div>");
            out.println("</div>");
            out.println("</section>");
            out.println("</body>");
            out.println("</html>");



       
    %>
<!--    
    <br />
        <a href="createcomment.jsp">Report Again</a>

        <input type="submit" id="btnSubmit" name="btnSubmit" value="Clear Report History"/>
        
    </body>
</html>
-->

