<%-- 
    Document   : SupportLogin
    Created on : 30-May-2017, 10:17:38
    Author     : Caleb Hoffman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- LOGIN PAGE FOR EVERYONE -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>Login Page</title>
    </head>
    <body>
        <!--
        <h1>Login: </h1>
        
        <form action="Validation" method="post">  
            Username:<input type="text" name="username"/><br/><br/>  
            Password:<input type="password" name="userpass"/><br/><br/>  
            <input type="submit" value="Login"/>  
        </form>  
        -->
                
       <section class="hero is-fullheight is-light is-bold"> 
           <br>
       <div class="container">         
           <div class="columns is-vcentered"> 
                <div class="column is-4 is-offset-4">
                <h1 class="title">Login</h1>
            <form action='Validation' method='post'> 
                <div class="box">
    
                <div class="field">
                    <label class="label">Username</label>
                    <p class="control">
                      <input class="input" type="text" name="username" placeholder="Username">
                    </p>      
                </div>
                
                <div class="field">
                    <label class="label">Password</label>
                    <p class="control">
                      <input class="input" type="password" name="userpass" placeholder="Password">
                    </p>      
                </div>

        
                <br />

                <input class="button is-primary" type="submit" value="Login"/>
                <a class="button is-default" href="home.jsp">Cancel</a>
                </div>
                
            </form>
        
        <br />        
        
        
                    
                    
                </div>
           </div>  
            

           
        </div>
        <br />
         </section>
    </body>
</html>
