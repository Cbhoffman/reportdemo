<%-- 
    Document   : userRequest
    Created on : 22-Jul-2017, 18:26:49
    Author     : Caleb Hoffman
--%>

<%@page import="SystemInteraction.ReportTypes"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>Support Team Request</title>
    </head>
    <body>
        <section class="hero is-fullheight is-light is-bold">
        <br>
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column is-two-thirds is-offset-2">
        
        <div class="level">            
            <h1 class="title">Be part of the Support Team</h1>
            <form action='Logout'>      
                <input type='submit' class='button is-light' value='Logout' />
            </form>
        </div>
            
        <div class="box">
        
        <p class="subtitle">This system can always use more workers to resolve issues around the UEA campus.</p>
        <p>If you are interested in becoming someone that can contribute to resolving different issues, please fill out the short form below, which will be forwarded to admin for approval.</p>
        <br />
        <label class="label">Please select the support area that you would like to join.</label>
        <form name= 'requestForm' action='UserRequest' method='post' onSubmit="return validate()" >
                <%
                  List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                  for(ReportTypes.Type type : types)
                  { 
                      String value = type.id;
                      String insert = type.name;
                      
               // out.print(value + " "+insert);
                out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                
                      //type.id;
                      //type.name;
                  }
                %>
                <br />
               

                <label class="label"> A brief description of why you should be considered or any relevant qualifications for the chosen report area. </label>
               
                <textarea class="textarea" cols="30" rows="4" name="requestDetails"></textarea><br />
                
                <div class="level">
                <input type="submit" class="button is-primary" value="Submit Your Request to Admin"/>
                <a class='button is-primary is-outlined' href='UserView?page=1'>Return to Profile Page</a>
                </div>
        </form>
                </div>
                <br />
                
                
                </div>
                </div>
                </div>
        </section>
    </body>
</html>
