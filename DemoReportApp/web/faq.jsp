<%-- 
    Document   : faq
    Created on : 26-Jul-2017, 13:05:15
    Author     : Caleb Hoffman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>FAQ Page</title>
    </head>
    <body>
        <div class="container">
        <nav class="nav has-shadow">
            <div class="container">
            <!--    <div class="nav-left">
                    <h1 class="nav-item">
                       UEA Reporting Portal  
                    </h1>
                </div>-->
                <span class="nav-toggle">
                  <span></span>
                  <span></span>
                  <span></span>
                </span>
                <div class="nav-right nav-menu">
                  <a href="home.jsp" class="nav-item is-tab">
                    Home
                  </a>
                  <a href="faq.jsp" class="nav-item is-tab is-active">
                    FAQ
                  </a>
                    <span class="nav-item">
                      <a href="userReg.jsp" class="button is-info">
                        Register
                      </a>
                    </span>
                    <span class="nav-item">
                      <a href="SupportLogin.jsp" class="button">
                        Log in
                      </a>
                    </span>
                </div>
            </div>
        </nav>
        <section class='hero is-primary is-medium'> 
            <div class='hero-body'>
                <div class='container has-text-centered'>
                    <h1 class='title is-2'>
                        Frequently Asked Questions
                    </h1>
                </div>    
            </div>
        </section>
                    
        <section class='section blog'> 
            <div class='container'>
                <div class='columns '>
                    <div class='column is-8 is-offset-2'>
                        <div class='content blog-post section'>
            
        <h1>What is the UEA Reporting Portal?</h1>
        <p>A single access point to report information around campus. If you have something to say but don't know where to say it, you have come to the right place.</p>
        <h1>Why is this needed?</h1>
        <p>Currently, there are different types of issues around campus someone might come across in their time at UEA, whether you are a student or staff member. However, in order to report an issue, you have to find the right people, office or location that is relevant to your specific problem in order to inform those who would resolve your particular issue. </p>
        <p> With this problem in mind, the UEA Reporting Portal was created to become a place where you can report a range of different issues from one simple location.</p>
        <h1>I am a first time user. How do you use this portal?</h1>
        <ol>
            <li><a href='userReg.jsp'>Register your details</a> in order create an account in the system.</li>
            <li>Once you have registered, you can then click 'Submit a Report'.</li>
            <li>Fill in the form and press 'Submit'</li>
        </ol>
        <p>After registering, you can log into the system with your personal username and password in order to view your report status, or to submit additional reports.</p>
        <h1>What happens when a report is submitted?</h1>
        <p>The information will be forwarded to the appropriate member of the support team. With the information you provide, these individuals will be able to resolve your issue in a suitable manner.</p>
        
        <h1>Can I track the status of my submitted report?</h1>
        <p>Yes, you can view the status of your report by selecting the 'View Your Previously Submitted Reports' button on your profile page.</p>
        
        <h1>How can I help resolve issues?</h1>
        <p>If you would like to be involved in resolving issues around campus you can request to become part of the support team. This can be achieved through selecting the 'Join the Support Team' button on your personal profile page. </p>
        </div>
        </div>
        </div>
               </div>
        </section>
                    
            <div class="hero-foot">
        <div class="container">
            <div class="tabs is-centered">
                <ul>
                <li class="is-active"><a href="http://www.uea.ac.uk">Image via UEA</a></li>
                <li><a>Project by Caleb Hoffman 2017</a></li>
                </ul>
            </div>
        </div>
    </div>            
                
        </div>

        <script async type="text/javascript" src="bulma.js"></script>
    </body>
</html>
