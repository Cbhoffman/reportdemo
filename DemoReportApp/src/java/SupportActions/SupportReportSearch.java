/*
 * NOT USING...DELETE?
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupportActions;

import SystemInteraction.Report;
import SystemInteraction.ReportManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "SupportReportSearch", urlPatterns = {"/SupportReportSearch"})
public class SupportReportSearch extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
                response.setContentType("text/html");  
        
        PrintWriter out=response.getWriter();  
        
        // Retrives the keyword to be searched...
        String searchKeyword = request.getParameter("searchKeyword");
        // Puts the search term into the correct format to be passed into the method further below.
        String searchKeywordFormat = searchKeyword.substring(1);
        
        // Retrieves the existing session that is created within the Validation.java servlet (when the support user logs in)     
        String reportType = (String) request.getSession(false).getAttribute("reportType");
        
        // Retrieves the number of the page that is clicked.
        String spageid=request.getParameter("page");
        
        // This initially sets the page value to one when support users log in.
        if (spageid == null)
        {
            spageid="1";
        }
        
        // Code below works out the correct amount of LIMIT and OFFSET for the SQL statement needed.
        int pageid=Integer.parseInt(spageid); 
        int resultsPerPage=5;  
        int offsetAmount;
        if(pageid==1)
        { 
            offsetAmount = 0;
        } else{  
            pageid=pageid-1;  
            offsetAmount=pageid*resultsPerPage;   
        }  
        
        int countOfRecords;
        List<Report> list;

        // The code below gathers all of the reports. 
            list=ReportManager.getAllRecords(resultsPerPage, offsetAmount,reportType,searchKeywordFormat); 
            countOfRecords = list.size();
        
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
        out.println("<title>Search Reports</title>");   
        out.println("<link rel='stylesheet' href='bulma.css'>");
        out.println("<style>#map{height: 400px; width: 400px; padding: 10%; position: absolute; visibility:hidden;}</style>");     
        out.println("<script src='reportjs.js' type='text/javascript'></script>");
        out.println("</head>");
        out.println("<body>");     
        out.println("<section class='hero is-fullheight is-light is-bold'>");
        out.println("<br>");
        out.println("<div class='container'>");
        out.println("<div class='columns is-vcentered'>");
        out.println("<div class='column is-14'>");
        out.println("<div class='level'>");  
        out.println("<h2 class='title' >Search Reports: "+searchKeyword+"</h2>"); 
        out.println("<form action='Logout'>");       
        out.println("<input type='submit' class='button is-light' value='Logout' />");
        out.println("</form>");
        out.println("</div>");
        
        if (list.size() == 0)
        {
            out.println("<div class='box'>");    
            out.println("<h2 class='title'>No Reports Found</h2>");
            out.println("<form action='NewSupportView'>"); 
            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>Main Reports Page</button>");  
            out.println("</form>");  
            out.println("</div>");
        } else {  
        out.println("<div class='box'>");
        out.println("<form action='NewSupportView'>"); 
        out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>Main Reports Page</button>");  
        out.println("</form>");  
        out.println("<br>");
        out.println("<form>");
        out.println("<table class='table box'>");  
        out.println("<thead><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></thead>"); 
        out.println("<tfoot><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></tfoot>"); 
        out.println("<tbody>");
        
        for(Report e:list)
        {  
            String dateFormatted = (String) e.getTimeOccured().subSequence(0, 19);
            out.println("<tr><td><input type='checkbox'  name='reports' value='"+e.getId()+"'></td><td>"+dateFormatted+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getDetails()+"</td><td><a href='"+e.getImg()+"' alt='Report Image' target='_blank' style='display: inline-block; width: 50px; height; 50px; background-image: url('"+e.getImg()+"');'>View Image</a></td><td>"+e.getStatus()+"</td><td><button class='locButton button is-info' id='location"+e.getId()+"' type='button' onclick='mapDisplay()' value='"+e.getGeolocation()+"' />View</button></td><td>"+e.getUrgency()+"</td></tr>");    
        }  
        out.println("</tbody>");
        out.println("</table>");  
        
        // Code below creates the paging for viewing the report results.
        int originalPage = Integer.parseInt(spageid);
        int totalPages = countOfRecords/resultsPerPage + 1;

      out.println("<br>");
      out.println("<nav class='pagination'>") ;
      if (originalPage == 1){
          out.println("<a class='pagination-previous' disabled>Previous</a>");
      } else {
          out.println("<a class='pagination-previous' href='SupportReportSearch?page="+(originalPage-1)+"'>Previous</a>");
      }
      if (originalPage == totalPages){
          out.println("<a class='pagination-next' disabled>Next page</a>");
      } else {
          out.println("<a class='pagination-next' href='SupportReportSearch?page="+(originalPage+1)+"'>Next page</a>");
      }

      out.println("<ul class='pagination-list'>");
      
        for (int i=1; i < totalPages +1; i++)
            {
                out.println("<li>");
                if (originalPage == i){
                    out.println("<a class=\"pagination-link is-current\" href='SupportReportSearch?page="+i+"'>"+i+"</a> ");  
                } else {
                    out.println("<a class=\"pagination-link\" href='SupportReportSearch?page="+i+"'>"+i+"</a> ");  
                }
                out.println("</li>");
            }
        out.println("</ul>");
        out.println("</nav>");
        out.println("<br>");
        out.println("<input type='submit' class='button is-primary' formaction='StatusChange' value='Change Status to Resolved' />");
        out.println("<input type='submit' class='button is-primary' formaction='ReportInwork' value='Change Status to In Work' />");
        out.println("</form>");           
        out.println("</div>");
    }
        out.println("<div class='box' id='map'></div>");
        out.println("</div>");
        out.println("</div>");
        out.println("</div>");
        out.println("<br>");
        out.println("</section>");
        out.println("<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCOPxPMdYe5Idujr5DFftHLZO_jf2XU92I&callback=initMap'></script>");
        out.println("</body>");
        out.println("</html>");
        out.close(); 
   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
//response.setContentType("text/html");  
//        
//        PrintWriter out=response.getWriter();  
//        
//        // Retrives the keyword to be searched...
//        String searchKeyword = request.getParameter("searchKeyword");
//        // Puts the search term into the correct format to be passed into the method further below.
//        String searchKeywordFormat = searchKeyword.substring(1);
//        
//        // Retrieves the existing session that is created within the Validation.java servlet (when the support user logs in)     
//        String reportType = (String) request.getSession(false).getAttribute("reportType");
//        
//        // Retrieves the number of the page that is clicked.
//        String spageid=request.getParameter("page");
//        
//        // This initially sets the page value to one when support users log in.
//        if (spageid == null)
//        {
//            spageid="1";
//        }
//        
//        // Code below works out the correct amount of LIMIT and OFFSET for the SQL statement needed.
//        int pageid=Integer.parseInt(spageid); 
//        int resultsPerPage=5;  
//        int offsetAmount;
//        if(pageid==1)
//        { 
//            offsetAmount = 0;
//        } else{  
//            pageid=pageid-1;  
//            offsetAmount=pageid*resultsPerPage;   
//        }  
//        
//        int countOfRecords;
//        List<Report> list;
//
//        // The code below gathers all of the reports. 
//            list=ReportManager.getAllRecords(resultsPerPage, offsetAmount,reportType,searchKeywordFormat); 
//            countOfRecords = list.size();
//        
//        out.println("<!DOCTYPE html>");
//        out.println("<html>");
//        out.println("<head>");
//        out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
//        out.println("<title>Search Reports</title>");   
//        out.println("<link rel='stylesheet' href='bulma.css'>");
//        out.println("<style>#map{height: 400px; width: 400px; padding: 10%; position: absolute; visibility:hidden;}</style>");     
//        out.println("<script src='reportjs.js' type='text/javascript'></script>");
//        out.println("</head>");
//        out.println("<body>");     
//        out.println("<section class='hero is-fullheight is-light is-bold'>");
//        out.println("<br>");
//        out.println("<div class='container'>");
//        out.println("<div class='columns is-vcentered'>");
//        out.println("<div class='column is-14'>");
//        out.println("<div class='level'>");  
//        out.println("<h2 class='title' >Search Reports: "+searchKeyword+"</h2>"); 
//        out.println("<form action='Logout'>");       
//        out.println("<input type='submit' class='button is-light' value='Logout' />");
//        out.println("</form>");
//        out.println("</div>");
//        
//        if (list.size() == 0)
//        {
//            out.println("<div class='box'>");    
//            out.println("<h2 class='title'>No Reports Found</h2>");
//            out.println("<form action='NewSupportView'>"); 
//            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>Main Reports Page</button>");  
//            out.println("</form>");  
//            out.println("</div>");
//        } else {  
//        out.println("<div class='box'>");
//        out.println("<form action='NewSupportView'>"); 
//        out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>Main Reports Page</button>");  
//        out.println("</form>");  
//        out.println("<br>");
//        out.println("<form>");
//        out.println("<table class='table box'>");  
//        out.println("<thead><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></thead>"); 
//        out.println("<tfoot><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></tfoot>"); 
//        out.println("<tbody>");
//        
//        for(Report e:list)
//        {  
//            String dateFormatted = (String) e.getTimeOccured().subSequence(0, 19);
//            out.println("<tr><td><input type='checkbox'  name='reports' value='"+e.getId()+"'></td><td>"+dateFormatted+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getDetails()+"</td><td><a href='"+e.getImg()+"' alt='Report Image' target='_blank' style='display: inline-block; width: 50px; height; 50px; background-image: url('"+e.getImg()+"');'>View Image</a></td><td>"+e.getStatus()+"</td><td><button class='locButton button is-info' id='location"+e.getId()+"' type='button' onclick='mapDisplay()' value='"+e.getGeolocation()+"' />View</button></td><td>"+e.getUrgency()+"</td></tr>");    
//        }  
//        out.println("</tbody>");
//        out.println("</table>");  
//        
//        // Code below creates the paging for viewing the report results.
//        int originalPage = Integer.parseInt(spageid);
//        int totalPages = countOfRecords/resultsPerPage + 1;
//
//      out.println("<br>");
//      out.println("<nav class='pagination'>") ;
//      if (originalPage == 1){
//          out.println("<a class='pagination-previous' disabled>Previous</a>");
//      } else {
//          out.println("<a class='pagination-previous' href='SupportReportSearch?page="+(originalPage-1)+"'>Previous</a>");
//      }
//      if (originalPage == totalPages){
//          out.println("<a class='pagination-next' disabled>Next page</a>");
//      } else {
//          out.println("<a class='pagination-next' href='SupportReportSearch?page="+(originalPage+1)+"'>Next page</a>");
//      }
//
//      out.println("<ul class='pagination-list'>");
//      
//        for (int i=1; i < totalPages +1; i++)
//            {
//                out.println("<li>");
//                if (originalPage == i){
//                    out.println("<a class=\"pagination-link is-current\" href='SupportReportSearch?page="+i+"'>"+i+"</a> ");  
//                } else {
//                    out.println("<a class=\"pagination-link\" href='SupportReportSearch?page="+i+"'>"+i+"</a> ");  
//                }
//                out.println("</li>");
//            }
//        out.println("</ul>");
//        out.println("</nav>");
//        out.println("<br>");
//        out.println("<input type='submit' class='button is-primary' formaction='StatusChange' value='Change Status to Resolved' />");
//        out.println("<input type='submit' class='button is-primary' formaction='ReportInwork' value='Change Status to In Work' />");
//        out.println("</form>");           
//        out.println("</div>");
//    }
//        out.println("<div class='box' id='map'></div>");
//        out.println("</div>");
//        out.println("</div>");
//        out.println("</div>");
//        out.println("<br>");
//        out.println("</section>");
//        out.println("<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCOPxPMdYe5Idujr5DFftHLZO_jf2XU92I&callback=initMap'></script>");
//        out.println("</body>");
//        out.println("</html>");
//        out.close();  
//    }
    }

    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
}

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
