/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupportActions;

import SystemInteraction.LoginValidation;
import static SystemInteraction.LoginValidation.validatePassword;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This servlet validates the support login information.
 * @author Caleb Hoffman
 */
@WebServlet(name = "Validation", urlPatterns = {"/Validation"})
public class Validation extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // If successful, the user will go to support.jsp, if not they will be asked to provide the correct information.
        // This log in process was created with help of following a tutorial at javatpoint.com.
        {  
            response.setContentType("text/html");  
            PrintWriter out = response.getWriter();  
            String n=request.getParameter("username");  
            String p=request.getParameter("userpass");  
            
            String storedPassword = LoginValidation.retrieveStoredPassword(n);
            
            try {
                // Validates username and password information entered into the form on home.jsp.
                // Determines where the user is directed.
                if(LoginValidation.validatePassword(p, storedPassword)){  
                    
                    // Creates a session if the login information is valid.
                    // This session is used for displaying the report information within the NewSupportView servlet.
                    
                    // get the user role
                    String userRole = LoginValidation.getUserRole(n);
                    
                    if ("admin".equals(userRole)) // if admin go to admin page
                    {
                        response.sendRedirect("AdminView");
                        
                    } else if ("user".equals(userRole)) // if user go to UserView
                    {
                        HttpSession user = request.getSession(true);
                        // Below returns the current user id to place into the following session.
                        int currentUser = LoginValidation.getCurrentUser(n);
                        user.setAttribute("user_id", currentUser);
                        response.sendRedirect("UserView");
                        
                    } else if ("support".equals(userRole)) // if support go to NewSupportPage
                    {
                        HttpSession ses = request.getSession(true);
                        String userReportType = LoginValidation.reportTypeForUser(n);
                        ses.setAttribute("reportType",userReportType);
                        response.sendRedirect("NewSupportView");
                        
                    }
                    
                } else {
                    // If the username or password is not valid, they will be forwarded to the SupportLogin.jsp.
                    out.print("Please enter correct username and password");
                    RequestDispatcher rd=request.getRequestDispatcher("SupportLogin.jsp");
                    rd.include(request,response);
                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, ex);
            }  
            out.close();  
        }  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
