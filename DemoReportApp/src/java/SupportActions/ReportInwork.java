/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupportActions;

import SystemInteraction.ReportManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "ReportInwork", urlPatterns = {"/ReportInwork"})
public class ReportInwork extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

                // Retrieves selected report ids (one or many) from NewSupportView checkboxes named 'reports' and creates a String array.
                String[] selectedReport = request.getParameterValues("reports");

                        // If the String array is not empty it will run the code below...
                        if (selectedReport != null)
                        {
                            // Loops through the String array...
                            for (String s : selectedReport)
                            {
                                // Runs the method within ReportManager class that changes the status to 'In Work' according to the id ('s' parameter).
                                ReportManager.setStatusInWork(s);
                            }       
                                // Displays the following HTML...
                                out.println("<!DOCTYPE html>");
                                out.println("<html>");
                                out.println("<head>");
                                out.println("<title>Servlet Status Change</title>"); 
                                out.println("<link rel='stylesheet' href='bulma.css'>");
                                out.println("</head>");
                                out.println("<body>");
                                out.println("<section class='hero is-fullheight is-light is-bold'>");
                                out.println("<br>");
         
                                out.println("<div class='container has-text-centered'>");
                                out.println("<div class='container is-centered box'>");

                                out.println("<h1 class='title is-spaced'>Report Status Change</h1>");
                                out.println("<h2 class='subtitle'>You have changed the status of selected reports to In Work.</h2>");
                                out.println("<a href='NewSupportView?page=1'>Return to Reports Page</a><br /><br />");
                                out.println("<form action='Logout'>");       
                                out.println("<input type='submit' class='button is-light' value='Logout' />");
                                out.println("</form>");
                                out.println("</div>");                           
                                out.println("</div>");
                                out.println("<br>");
                                out.println("</section>");
                                out.println("</body>");
                                out.println("</html>");            
                        } 
                        // If the String array is empty, it will prompt the support user to select a report...
                        else {
                            out.println("Please select a report in order to update the status to In Work.");
                        }           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StatusChange.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(StatusChange.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StatusChange.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(StatusChange.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
