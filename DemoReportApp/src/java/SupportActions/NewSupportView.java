
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupportActions;

import SystemInteraction.Report;
import SystemInteraction.ReportManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "NewSupportView", urlPatterns = {"/NewSupportView"})
public class NewSupportView extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            response.setContentType("text/html");  
        
        PrintWriter out=response.getWriter();  
       
        // Retrieves the existing session that is created within the Validation.java servlet (when the support user logs in)     
        String reportType = (String) request.getSession(false).getAttribute("reportType");
        
        //////out.print("Session is: "+reportType+"\n");
        String searchInput = request.getParameter("searchInput");
        //////out.println("The search parameter is: "+ searchInput);
        // Retrieves the number of the page that is clicked.
        String spageid=request.getParameter("page");
        
        // Retrives the value of the buttons "View Resolved Reports" and "View Active Reports"
        String viewResolved = request.getParameter("viewResolved");  
       
        // Creates a new session based on whether the support user selects the "View Resolved Reports" or "View Active Reports" button.
        // If the button is selected, it will create the session that allows the user to browse the pages. It will skip if the user 
        // views page two of the reports selected.
        if (viewResolved != null)
            {
                HttpSession sessionResolvedView = request.getSession(true);                             
                sessionResolvedView.setAttribute("viewResolvedReps",viewResolved);                  
            }
        
        // Retrieves the session created when the user wants to view either the active or resolved reports.
        String viewRes = (String) request.getSession(false).getAttribute("viewResolvedReps");
                
        ////out.println(viewRes);
        
        // This initially sets the page value to one when support users log in.
        if (spageid == null)
        {
            spageid="1";
        }
        
        // Code below works out the correct amount of LIMIT and OFFSET for the SQL statement needed.
        int pageid=Integer.parseInt(spageid); 
        int resultsPerPage=5;  
        int offsetAmount;
        if(pageid==1)
        { 
            offsetAmount = 0;
        } else{  
            pageid=pageid-1;  
            offsetAmount=pageid*resultsPerPage;   
        }  
        
        int countOfRecords;
        List<Report> list;
        // Below is code when the support user selects button to view Resolved Reports. 
        // "viewResolved" is the value within the "View Resolved Reports" button. "viewRes" is the session created earlier.
        if ("viewResolved".equals(viewRes))
        {
            countOfRecords = ReportManager.getNumberofResolvedRecords(reportType);
            list = ReportManager.getResolvedRecords(resultsPerPage, offsetAmount,reportType);
        ////    out.print(countOfRecords);
        } else {
        // The code below gathers the Active reports. 
            countOfRecords = ReportManager.getNumberofActiveRecords(reportType);
            list=ReportManager.getActiveRecords(resultsPerPage, offsetAmount,reportType); 
        ////    out.print(countOfRecords);
        }
        
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
        out.println("<title>Previous Reports</title>");   
        out.println("<link rel='stylesheet' href='bulma.css'>");
        out.println("<style>#map{height: 400px; width: 400px; padding: 10%; position: absolute; visibility:hidden;}</style>");
    //    out.println("<style>#img{height: 400px; width: 400px; padding: 10%; position: absolute; visibility:hidden;}</style>");       
        out.println("<script src='reportjs.js' type='text/javascript'></script>");
        out.println("</head>");
        out.println("<body>");     
        out.println("<section class='hero is-fullheight is-light is-bold'>");
        out.println("<br>");
        out.println("<div class='container'>");
        out.println("<div class='columns is-vcentered'>");
        out.println("<div class='column is-14'>");

      //  out.println("<h1 class='title' >Page No: "+spageid+"</h1>");
        
        if ("viewResolved".equals(viewRes)){
        out.println("<div class='level'>");    
        out.println("<h2 class='title' >Resolved Reports</h2>"); 
        out.println("<form action='Logout'>");       
        out.println("<input type='submit' class='button is-light' value='Logout' />");
        out.println("</form>");
        out.println("</div>");
        } else {
        out.println("<div class='level'>");  
        out.println("<h2 class='title' >Active Reports</h2>"); 
        out.println("<form action='Logout'>");       
        out.println("<input type='submit' class='button is-light' value='Logout' />");
        out.println("</form>");
        out.println("</div>");}
                

        
        if (list.size() == 0)
        {
            if("viewResolved".equals(viewRes)){
            out.println("<div class='box'>");    
            out.println("<h2 class='title'>No Resolved Reports to display</h2>");
            out.println("<form action='NewSupportView'>"); 
            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>View Active Reports</button>");  
            out.println("</form>");  
            out.println("</div>");
            } else {
            out.println("<div class='box'>");    
            out.println("<h2 class='title'>No Active Reports to display</h2>");
            out.println("<form action='NewSupportView'>"); 
            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewResolved'>View Resolved Reports</button>");  
            out.println("</form>");  
            out.println("</div>");}
          //  out.println("<br>");
        } else {  
       
        
        
        
        if (viewRes == null)
        {
            viewRes = "viewActive";
        } 
        
        out.println("<div class='container'>");
        out.println("<div class='columns'>");
        out.println("<div class='column is-3'>");
        
        if ("viewActive".equals(viewRes))
        {
            out.println("<form action='NewSupportView'>"); 
            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewResolved'>View Resolved Reports</button>");  
            out.println("</form>");
            out.println("<br>");
        } else if ("viewResolved".equals(viewRes))
        {
            out.println("<form action='NewSupportView'>"); 
            out.println("<button class='button is-info' name='viewResolved' type='submit' value='viewActive'>View Active Reports</button>");  
            out.println("</form>");  
            out.println("<br>");
        } else {
            viewRes = "viewActive";
        }
        out.println("</div>");

        out.println("<div class='column is-3'>");
        out.println("<form name= 'reportSearch' action='SupportReportSearch' method='get'>");
        out.println("<div class='level'>");
        out.println("<input class='input' type='text' name='searchKeyword' placeholder='keyword'>");
        out.println("<input class='button is-primary' type='submit' value='Search'>");
        out.println("</div>");
        out.println("</form>");
        out.println("</div>");
        out.println("</div>");
        out.println("</div>");
        out.println("<br />");
        
        
         out.println("<div class='box'>");
        
        out.println("<form>");
        
        out.println("<input type='submit' class='button is-primary' formaction='StatusChange' value='Change Status to Resolved' />");
        out.println("<input type='submit' class='button is-primary' formaction='ReportInwork' value='Change Status to In Work' />");
        out.println("<br />");
        out.println("<br />");

       // out.println("<table class='table box container' border='1' cellpadding='4' width='60%'>");  
       
        out.println("<table class='table box'>");  
        //out.println("<thead><tr><th></th><th>Id</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Report Type</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></thead>");  
        out.println("<thead><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></thead>"); 
        out.println("<tfoot><tr><th>Select</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></tfoot>"); 
        out.println("<tbody>");
        
        
    
        for(Report e:list)
        {  
            String dateFormatted = (String) e.getTimeOccured().subSequence(0, 19);
            
            //out.println("<tr><td><input type='checkbox' name='reports' value='"+e.getId()+"'></td><td>"+e.getId()+"</td><td>"+e.getTimeOccured()+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getReportType()+"</td><td>"+e.getDetails()+"</td><td><img class=rpImg style='width:200px;height:200px;' src="+e.getImg()+"></td><td>"+e.getStatus()+"</td><td><button class='locButton' id='location"+e.getId()+"' type='button' onclick='mapDisplay()' value='"+e.getGeolocation()+"' />View Location</button></td><td>"+e.getUrgency()+"</td></tr>");     
            out.println("<tr><td><input type='checkbox'  name='reports' value='"+e.getId()+"'></td><td>"+dateFormatted+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getDetails()+"</td><td><a href='"+e.getImg()+"' alt='Report Image' target='_blank' style=display: inline-block; width: 50px; height; 50px; background-image: url('"+e.getImg()+"');>View Image</a></td><td>"+e.getStatus()+"</td><td><button class='locButton button is-info' id='location"+e.getId()+"' type='button' onclick='mapDisplay()' value='"+e.getGeolocation()+"' />View</button></td><td>"+e.getUrgency()+"</td></tr>");    
        }  
        out.println("</tbody>");
        out.println("</table>");  
        
                // Code below creates the paging for viewing the report results.
        int originalPage = Integer.parseInt(spageid);
        int totalPages = countOfRecords/resultsPerPage + 1;
      //for (int i=1; i < totalPages +1; i++) ORIGINAL
      out.println("<br>");
      out.println("<nav class='pagination'>") ;
      if (originalPage == 1){
          out.println("<a class='pagination-previous' disabled>Previous</a>");
      } else {
          out.println("<a class='pagination-previous' href='NewSupportView?page="+(originalPage-1)+"'>Previous</a>");
      }
      if (originalPage == totalPages){
          out.println("<a class='pagination-next' disabled>Next page</a>");
      } else {
          out.println("<a class='pagination-next' href='NewSupportView?page="+(originalPage+1)+"'>Next page</a>");
      }

      out.println("<ul class='pagination-list'>");
      
        for (int i=1; i < totalPages +1; i++)
            {
                out.println("<li>");
                if (originalPage == i){
                    out.println("<a class=\"pagination-link is-current\" href='NewSupportView?page="+i+"'>"+i+"</a> ");  
                } else {
                    out.println("<a class=\"pagination-link\" href='NewSupportView?page="+i+"'>"+i+"</a> ");  
                }
                out.println("</li>");
            }
        out.println("</ul>");
        out.println("</nav>");
    //    }
        out.println("<br />");
        
       
        
        out.println("<input type='submit' class='button is-primary' formaction='StatusChange' value='Change Status to Resolved' />");
        out.println("<input type='submit' class='button is-primary' formaction='ReportInwork' value='Change Status to In Work' />");

      
        out.println("</form>");     

        
        out.println("<br />");
        

        
        out.println("</div>");
        
    }

        out.println("<div class='box' id='map'></div>");

        out.println("</div>");
        out.println("</div>");
        out.println("</div>");
        out.println("<br>");
        out.println("</section>");
        
        out.println("<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCOPxPMdYe5Idujr5DFftHLZO_jf2XU92I&callback=initMap'></script>");
        
        out.println("</body>");
        out.println("</html>");
    
        out.close();  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}