// Currently, this servlet is no longer necessary. However can be archived for possible future use.

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archive;

import SystemInteraction.Report;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "SupportView", urlPatterns = {"/SupportView"})
public class SupportView extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Connection c = null;
        Statement stmt = null;
        
        String viewReports = request.getParameter("searchType");
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            Class.forName("org.postgresql.Driver");
            c = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "kc2285280");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();

        //    String sql = "select * from report where reporttype = '"+viewReports+"';";
            
            String sql = "SELECT reporttype, report.id, timeoccured, fname, lname, contact, email, details, img, status, geolocation, urgency "+
            "FROM report LEFT JOIN addtags ON report.ID = addtags.ID WHERE reporttype = '"+viewReports+"' ORDER BY report.id;";
            
            ResultSet myRs = stmt.executeQuery(sql);
            ArrayList searchResults = new ArrayList();
            ArrayList selectedReportIds = new ArrayList();
            ArrayList geolocations = new ArrayList();
            ArrayList<Report> reportList = new ArrayList<Report>();

            //while loop adds the relevant information into one array, and the selected id into a separate array in order to use that for changing the status below.

//                while(myRs.next()){
//                    searchResults.add("<img class=rpImg style='width:200px;height:200px;' src="+myRs.getString("img")+">"+myRs.getTimestamp("timeoccured")+": "+ myRs.getString("fname")+" "+ myRs.getString("lname")+": "+
//                    myRs.getString("details")+" -- "+myRs.getString("email")+" "+"ID#: "+myRs.getInt("id")+" "+"Status: "+myRs.getString("status")+" Priority: "+myRs.getString("urgency"));
//                    selectedReportIds.add(myRs.getInt("id"));
//                    geolocations.add(myRs.getString("geolocation"));
//                }
                
                while(myRs.next()){
                    
                    
                    reportList.add( new Report(myRs.getInt("id"),myRs.getTimestamp("timeoccured").toString(),myRs.getString("fname"),myRs.getString("lname"),myRs.getString("contact"), myRs.getString("email"), viewReports, myRs.getString("details"), myRs.getString("img"), myRs.getString("status"), myRs.getString("geolocation"), myRs.getString("urgency") ) );                   
                    
//                    searchResults.add("<img class=rpImg style='width:200px;height:200px;' src="+myRs.getString("img")+">"+myRs.getTimestamp("timeoccured")+": "+ myRs.getString("fname")+" "+ myRs.getString("lname")+": "+
//                    myRs.getString("details")+" -- "+myRs.getString("email")+" "+"ID#: "+myRs.getInt("id")+" "+"Status: "+myRs.getString("status")+" Priority: "+myRs.getString("urgency"));
                    selectedReportIds.add(myRs.getInt("id"));
//                    geolocations.add(myRs.getString("geolocation"));
                }
                
                

            stmt.close();

               out.println("<!DOCTYPE html>");
               out.println("<html>");
               out.println("<head>");
               out.println("<title>Previous Reports</title>");   
               out.println("<style>#map{height: 400px; width: 400px; padding: 10%; position: absolute; visibility:hidden;}</style>");
               out.println("</head>");
               out.println("<body>");
               out.println("<h1>Previously Submitted Reports</h1>");
               out.println("<h2>Report Type: "+viewReports+"</h2>");

               // out.println("<form action='StatusChange'>");
               out.println("<form>");
//////               for (int i = 0; i < searchResults.size(); i++)
//////                   
//////               {
//////                   //This is where the selected id is pulled from the separate array list of report ids.
//////                    out.println("<input type='checkbox' name='reports' value='"+selectedReportIds.get(i)+"'>"+searchResults.get(i)+"");
//////                    String noLoc = "not provided";       
//////                    if (noLoc.equals(geolocations.get(i))){
//////                         out.println("No location provided.");
//////                    } else {
//////                  //  out.println("<button id='location' type='button' onclick='mapDisplay()' value='"+geolocations.get(i)+"' />View Location</button>");
//////                  //  out.println(geolocations.get(i));
//////                    out.println("<button class='locButton' id='location"+selectedReportIds.get(i)+"' type='button' onclick='mapDisplay()' value='"+geolocations.get(i)+"' />View Location</button>");
//////                    }
//////                    out.println("<br /><br />");
//////               };

               for (int i = 0; i < reportList.size(); i++)
                   
               {
                   //This is where the selected id is pulled from the separate array list of report ids.
                    out.println("<img class=rpImg style='width:200px;height:200px;' src="+reportList.get(i).getImg()+">");
                    out.println("<input type='checkbox' name='reports' value='"+selectedReportIds.get(i)+"'>"+reportList.get(i).getId()+" "+ reportList.get(i).getTimeOccured()+" "+ reportList.get(i).getLname()+" "+ reportList.get(i).getFname()+" "+ reportList.get(i).getContact()+" "+ reportList.get(i).getEmail()+" "+ reportList.get(i).getDetails()+" "+ reportList.get(i).getStatus()+" "+ reportList.get(i).getUrgency());
                    String noLoc = "not provided";       
                    if (noLoc.equals(reportList.get(i).getGeolocation())){
                         out.println("No location provided.");
                    } else {
                    out.println("<button class='locButton' id='location"+selectedReportIds.get(i)+"' type='button' onclick='mapDisplay()' value='"+reportList.get(i).getGeolocation()+"' />View Location</button>");
                   
                    }
                    out.println("<br />");
               };
               
               out.println("<br /><br />");
               out.println("<input type='submit' formaction='StatusChange' value='Report Resolved' />");
               out.println("<input type='submit' formaction='ReportInwork' value='Report In Work' />");
               out.println("</form>");
               out.println("<div id='map'></div>");
               out.println("<br />");
               out.println("<a href='support.jsp'>Back to the Support Page</a><br /><br />");
               out.println("<script src='reportjs.js' type='text/javascript'></script>");
               out.println("<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCOPxPMdYe5Idujr5DFftHLZO_jf2XU92I&callback=initMap'></script>");
               out.println("</body>");
               out.println("</html>");
                        
        } catch ( Exception e ) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
       }
       System.out.println("Table created successfully");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}