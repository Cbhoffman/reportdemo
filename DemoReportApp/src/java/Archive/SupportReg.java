/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archive;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "SupportReg", urlPatterns = {"/SupportReg"})
public class SupportReg extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Support User Request</title>");            
            out.println("</head>");
            out.println("<body>");
    
            
            String fname = request.getParameter("fName");
            String lname = request.getParameter("lName");
            String section = request.getParameter("reportType");
            String contact = request.getParameter("contact");
            String email = request.getParameter("email");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            
            out.println("<h1>Thank you for registering as a support user.</h1>");
            out.println("<h2>Once you are verified by the system administrator you will be granted privileges to view reports when you log into the system.</h2>");
            out.println("<h3>Support Registration Details:</h3>");
            out.println("<p>"+fname+"</p>");
            out.println("<p>"+lname+"</p>");
            out.println("<p>"+section+"</p>");
            out.println("<p>"+contact+"</p>");
            out.println("<p>"+email+"</p>");
            out.println("<p>"+username+"</p>");
            out.println("<p>"+password+"</p>");
            
            out.println("</body>");
            out.println("</html>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    
//    response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Support User Request</title>");            
//            out.println("</head>");
//            out.println("<body>");
//    
//            
//            String fname = request.getParameter("fName");
//            String lname = request.getParameter("lName");
//            String section = request.getParameter("reportType");
//            String contact = request.getParameter("contact");
//            String email = request.getParameter("email");
//            String username = request.getParameter("username");
//            String password = request.getParameter("password");
//            
//            out.println("<h1>Thank you for registering as a support user.</h1>");
//            out.println("<h2>Once you are verified by the system administrator you will be granted privileges to view reports when you log into the system.</h2>");
//            out.println("<h3>Support Registration Details:</h3>");
//            out.println("<p>"+fname+"</p>");
//            out.println("<p>"+lname+"</p>");
//            out.println("<p>"+section+"</p>");
//            out.println("<p>"+contact+"</p>");
//            out.println("<p>"+email+"</p>");
//            out.println("<p>"+username+"</p>");
//            out.println("<p>"+password+"</p>");
//            
//            out.println("</body>");
//            out.println("</html>");
//        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
