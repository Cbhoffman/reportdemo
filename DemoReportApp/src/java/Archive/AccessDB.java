/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archive;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.mail.*;


/**
 *
 * @author Caleb Hoffman
 */

//PAGE NO LONGER NEEDED

@WebServlet(name = "AccessDB", urlPatterns = {"/AccessDB"})
public class AccessDB extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.apache.commons.mail.EmailException
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        try {
            response.setContentType("text/html;charset=UTF-8");
            Connection c = null;
            Statement stmt = null;
            String name = request.getParameter("name");
            String contact = request.getParameter("contact");
            String emailAddress = request.getParameter("email");
            String details = request.getParameter("text");
            
            String reportType = request.getParameter("reportType");
            
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                
//                Class.forName("org.postgresql.Driver");
//                c = DriverManager
//                        .getConnection("jdbc:postgresql://localhost:5432/postgres",
//                                "postgres", "kc2285280");
//                System.out.println("Opened database successfully");
//                
//                stmt = c.createStatement();
//                
//                String sql = "insert into report (name, contact, email, details, reporttype, status)"+
//                        " values ('"+name+"', '"+contact+"', '"+emailAddress+"','"+details+"','"+reportType+"','active'); ";
//                
//                
//                stmt.executeUpdate(sql);
//                stmt.close();
//                c.close();
            
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet AccessDB</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Thank you for your report.</h1>");
                out.println("<h2>You just submitted the following data:</h2>");
                out.println("<p>Name: "+name+"</p>");
                out.println("<p>Contact: "+contact+"</p>");
                out.println("<p>Email: "+emailAddress+"</p>");
                out.println("<p>Details: "+details+"</p>");
                out.println("<p>Report Type: "+reportType+"</p>");
                
////                out.println("<h2>Search for additional reports.</h2>");
////                out.println("<form name= 'report' action='Search' method='post' >");
////                //    out.println("Search By Name: <input type='text' name='search' placeholder='name' /><br />");
////                out.println("<br />");
////                out.println("<input type='radio' name='searchType' value='security'>Security<br>");
////                out.println("<input type='radio' name='searchType' value='it'>IT<br>");
////                out.println("<input type='radio' name='searchType' value='mtFac'>Maintenance and Facilities<br>");
////                out.println("<input type='radio' name='searchType' value='parking'>Parking and Transportation");
////                out.println("<br />");
////                out.println("<br />");
////                out.println(" <input type='submit' value='Search' />");
////                out.println("</form>");
                out.println("<br />");
                out.println("<a href='report.jsp'>Back to the Home Page</a><br /><br />");
                out.println("</body>");
                out.println("</html>");
                
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
                System.exit(0);
            }
            System.out.println("Table created successfully");
            
            //Sending an email to support personnel after report is submitted

            String authuser = "ueareport@gmail.com";
            String authpwd = "ueareporting";

            Email email = new SimpleEmail();
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(587);
            email.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
            email.setStartTLSEnabled(true);
            email.setFrom(authuser);
            email.setSubject("New Report Added");
            email.setMsg("A new report has been added by "+ name + " who said: "+ details);

            switch (reportType) {
                    case "security":
                        email.addTo("ueareport+security@gmail.com");
                        break;
                    case "it":
                        email.addTo("ueareport+it@gmail.com");
                        break;
                    case "mtFac":
                        email.addTo("ueareport+maintenance@gmail.com");
                        break;
                    case "parking":
                        email.addTo("ueareport+parking@gmail.com");
                        break;
                    case "union":
                        email.addTo("ueareport+studentunion@gmail.com");
                        break;
                    default:
                        break;
                }

            email.send();

                    } catch ( EmailException ex ) {
                        Logger.getLogger(AccessDB.class.getName()).log(Level.SEVERE, null, ex);
                   }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
 
}
