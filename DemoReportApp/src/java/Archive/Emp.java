/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archive;

// Created for testing, no longer needed...


/**
 * 
 * @author Caleb Hoffman
 */
public class Emp {
    
    
    private int id;  
    private String name;  
    private float salary;  

//getters and setters  
    
    public int getId() { return id; }
    public String getName() { return name; }
    public float getSalary() { return salary; }
    
    public void setId(int setId) { this.id = setId; }
    public void setName(String setName) { this.name = setName; }
    public void setSalary(float setSalary) { this.salary = setSalary; }

}
