/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Archive;

// Created for testing, no longer needed...


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Caleb Hoffman
 */
public class EmpDao {  
  
    public static Connection getConnection(){  
        Connection con=null;  
        try{  
            Class.forName("org.postgresql.Driver");  
            con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "kc2285280");  
        }catch(Exception e){System.out.println(e);}  
        return con;  
    }  
  
    public static List<Emp> getRecords(int total,int start){  
        List<Emp> list=new ArrayList<Emp>();  
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("select * from emp limit "+total+" offset "+start);  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                Emp e=new Emp();  
                e.setId(rs.getInt(1));  
                e.setName(rs.getString(2));  
                e.setSalary(rs.getFloat(3));  
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
    

    public static int getNumberofRecords()
    {
        int count = 0;
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("select count(*) from emp");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                count = rs.getInt(1);     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return count;
    }
   
    
}  
