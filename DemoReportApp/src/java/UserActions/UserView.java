/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserActions;

import SystemInteraction.LoginValidation;
import SystemInteraction.User;
import SystemInteraction.UserManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "UserView", urlPatterns = {"/UserView"})
public class UserView extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            // Retrieves the existing session that is created within the Validation.java servlet (when the support user logs in)     
        int user_id = (int) request.getSession(false).getAttribute("user_id");
         ////   out.println("<p>"+user_id+"</p>");
            
            User currentUser = UserManager.getUser(user_id);
            String fName = currentUser.getFname();
            String lName = currentUser.getLname();
            
            // Setting the current user within a session to be used with submitting a report (SubmitReport.java)
            HttpSession user = request.getSession(true);       
            user.setAttribute("user", currentUser);
            
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>User Page</title>");
            out.println("<link rel='stylesheet' href='bulma.css'>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<section class='hero is-fullheight is-default is-light is-bold'>");
    //        out.println("<div class='hero-head'>");
    //        out.println("<div class='container'>");
    //        out.println("<nav class='has-shadow'>");
    //        out.println("<div class='container'>");
    //        out.println("<div class='nav-right nav-menu'>");
    //        out.println("<a href='home.jsp' class='nav-item button'>Log Out</a>");
    //        out.println("</div>");
    //        out.println("</div>");
    //        out.println("</nav>");
    //        out.println("</div>");
    //        out.println("</div>");
            
            out.println("<div class='hero-body'>");
            out.println("<div class='container has-text-centered'>");
            out.println("<div class='columns is-vcentered'>");
            out.println("<div class='column is-4 is-offset-4'>");
            
            
            out.println("<h1 class='title is-spaced'>User Action Page</h1>");
    
            out.println("<h2 class='subtitle'>Hello "+fName+" "+lName+"</h2>");

            out.println("<div class= 'box'>");
            
            // Submit a New Report Button
            out.println("<form action='newReport.jsp'>"); 
            out.println("<button class='button is-primary' type='submit' value='newReport'>Submit a New Report</button>");  
            out.println("</form>");  
            
            out.println("<br>");
            
            out.println("<form action='UserViewReports'>"); 
            out.println("<button class='button is-primary' type='submit' value='userReports'>View Your Previously Submitted Reports</button>");  
            out.println("</form>");  
            
            out.println("<br>");
            
        // Request to be part of the Support Team
            out.println("<form action='userRequest.jsp'>"); 
            out.println("<button class='button is-info' type='submit' value='userRequest'>Be part of the Support Team</button>");  
            out.println("</form>");  
        
            out.println("<br>");
            
            out.println("<form action='Logout'>");       
            out.println("<input type='submit' class='button is-light' value='Logout' />");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");

            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            
           
            
            out.println("</section>");
            
            out.println("</body>");
            out.println("</html>");
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
