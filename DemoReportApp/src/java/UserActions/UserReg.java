/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserActions;

import SystemInteraction.LoginValidation;
import SystemInteraction.ReportManager;
import SystemInteraction.User;
import SystemInteraction.UserManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This is a servlet that processes the user information and displays confirmation of registration.
 * @author Caleb Hoffman
 */
@WebServlet(name = "UserReg", urlPatterns = {"/UserReg"})
public class UserReg extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, InvalidKeySpecException {
             response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {          
            String fName = request.getParameter("fName");
            String lName = request.getParameter("lName");
            String contact = request.getParameter("contact");
            String email = request.getParameter("email").toLowerCase();
            
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String confirmp = request.getParameter("confirmpassword");
            String userRole = "user";

            
            if (fName.isEmpty() == true){
                out.println("Please enter your first name.");
            } else if (lName.isEmpty() == true){
                out.println("Please enter your last name.");
            } else if (contact.length() != 11){
                out.println("Please enter a contact number with 11 digits.");
//            } else if (Pattern.matches("[a-zA-Z]+", contact) == false){
//                out.println("Please enter a valid contact number.");
            } else if (email.contains("@") == false ){
                out.println("Please enter a valid email address.");
            } else if (username.isEmpty() == true){
                out.println("Please enter a username.");
            } else if (password.isEmpty() == true){
                out.println("Please enter a password.");
            } else if(!password.equals(confirmp)){
                out.println("Passwords must match.");
            } else {

            // Create a hash with the password that has been provided by the user. Then store that value within the database.
             String securePassword = LoginValidation.generateStorngPasswordHash(password);
              
            // Creating a User object with given user information.
            User newUser = new User();
            newUser.setFname(fName);
            newUser.setLname(lName);
            newUser.setContact(contact);
            newUser.setEmail(email);
       
            newUser.setUser_Login(username);
            newUser.setPassword(securePassword);
            newUser.setRole(userRole);
            
            // Inserting the user information into the database.
            UserManager.insertUser(newUser);
            
            // Possibly retrieve created user id and create session (in case the user submits a report from here).
                HttpSession user = request.getSession(true);
                    // Below returns the current user id to place into the following session.
                    int currentUser = LoginValidation.getCurrentUser(username);
                    user.setAttribute("user_id", currentUser);
                ////    response.sendRedirect("UserView");  
            
            // HTML that displays registration confirmation to the user.
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel='stylesheet' href='bulma.css'>");
            out.println("<title>Completed Registation</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<section class='hero is-fullheight is-light is-bold'>");
            out.println("<br>");
            out.println("<div class='container box'>");
            out.println("<div class='columns is-vcentered'> ");
            out.println("<div class='column is-4 is-offset-4'>");
            out.println("<h1 class='title'>Registration Complete</h1>");
            out.println("<p>Thank you for registering with us "+fName+". "+"You can now submit a report.</p>");            
            out.println("<br />");
            out.println("<form action='UserView'>");
            out.println("<div class='level'>");
            out.println("<input class='button is-primary' type='submit' value='Go to your profile page' />");
            out.println("<a href='home.jsp' class='button'>Logout</a>");
            out.println("</div>");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("<br>");
            out.println("</section>");
         
            out.println("</body>");
            out.println("</html>");
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            
//            response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {          
//            String fName = request.getParameter("fName");
//            String lName = request.getParameter("lName");
//            String contact = request.getParameter("contact");
//            String email = request.getParameter("email").toLowerCase();
//            
//            String username = request.getParameter("username");
//            String password = request.getParameter("password");
//            String confirmp = request.getParameter("confirmpassword");
//            String userRole = "user";
//
//            
//            if (fName.isEmpty() == true){
//                out.println("Please enter your first name.");
//            } else if (lName.isEmpty() == true){
//                out.println("Please enter your last name.");
//            } else if (contact.length() != 11 || Pattern.matches("[a-zA-Z]+", contact) == false){
//                out.println("Please enter a contact number with 11 digits.");
//            } else if (email.contains("@") == false ){
//                out.println("Please enter a valid email address.");
//            } else if (username.isEmpty() == true){
//                out.println("Please enter a username.");
//            } else if (password.isEmpty() == true){
//                out.println("Please enter a password.");
//            } else if(!password.equals(confirmp)){
//                out.println("Passwords must match.");
//            } else {
//
//            // Create a hash with the password that has been provided by the user. Then store that value within the database.
//             String securePassword = LoginValidation.generateStorngPasswordHash(password);
//              
//            // Creating a User object with given user information.
//            User newUser = new User();
//            newUser.setFname(fName);
//            newUser.setLname(lName);
//            newUser.setContact(contact);
//            newUser.setEmail(email);
//       
//            newUser.setUser_Login(username);
//            newUser.setPassword(securePassword);
//            newUser.setRole(userRole);
//            
//            // Inserting the user information into the database.
//            UserManager.insertUser(newUser);
//            
//            // Possibly retrieve created user id and create session (in case the user submits a report from here).
//                HttpSession user = request.getSession(true);
//                    // Below returns the current user id to place into the following session.
//                    int currentUser = LoginValidation.getCurrentUser(username);
//                    user.setAttribute("user_id", currentUser);
//                ////    response.sendRedirect("UserView");
//            
//            // HTML that displays registration confirmation to the user.
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<link rel='stylesheet' href='bulma.css'>");
//            out.println("<title>Completed Registation</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<section class='hero is-fullheight is-light is-bold'>");
//            out.println("<br>");
//            out.println("<div class='container box'>");
//            out.println("<div class='columns is-vcentered'> ");
//            out.println("<div class='column is-4 is-offset-4'>");
//            out.println("<h1 class='title'>Registration Complete</h1>");
//            out.println("<p>Thank you for registering with us "+fName+". "+"You can now submit a report.</p>");            
//            out.println("<br />");
//            out.println("<form action='UserView'>");
//            out.println("<div class='level'>");
//            out.println("<input class='button is-primary' type='submit' value='Go to your profile page' />");
//            out.println("<a href='home.jsp' class='button'>Logout</a>");
//            out.println("</div>");
//            out.println("</form>");
//            out.println("</div>");
//            out.println("</div>");
//            out.println("</div>");
//            out.println("<br>");
//            out.println("</section>");
//         
//            out.println("</body>");
//            out.println("</html>");
//            }
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (InvalidKeySpecException ex) {
//            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(UserReg.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
