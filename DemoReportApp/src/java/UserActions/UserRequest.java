/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserActions;

import SystemInteraction.User;
import SystemInteraction.UserManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "UserRequest", urlPatterns = {"/UserRequest"})
public class UserRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, EmailException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            // Retrieves the existing session that is created within the UserView servlet (when the support user logs in)     
            int user_id = (int) request.getSession(false).getAttribute("user_id");
            
            //Retrives user details of the current user 
            User currentUser = UserManager.getUser(user_id);
            String fName = currentUser.getFname();
            String lName = currentUser.getLname();
            
            
            // Sending email to admin...
            String authuser = "ueareport@gmail.com";
            String authpwd = "ueareporting";
 
            String reportArea = request.getParameter("reportType");
            String text = request.getParameter("requestDetails");
            
            Email em = new SimpleEmail();
            em.setHostName("smtp.gmail.com");
            em.setSmtpPort(587);
            em.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
            em.setStartTLSEnabled(true);
            em.setFrom(authuser);
            em.setSubject("Request to be a Support User");
            em.setMsg(fName+" "+lName+" has requested to be a support user, please log in to change their role."+" Report Area selected: "+reportArea+ 
                    ". "+text+" "); 
            em.addTo("ueareport+admin@gmail.com");
            em.send();
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Request Sent</title>");  
            out.println("<link rel='stylesheet' href='bulma.css'>");
            out.println("</head>");
            out.println("<body>");
            out.println("<section class='hero is-fullheight is-light is-bold'>");
            out.println("<br>");
            out.println("<div class='container has-text-centered'>");
            out.println("<div class='columns is-vcentered'>");
            out.println("<div class='column is-6 is-offset-3'>");
            out.println("<h1 class='title'>Thank you for your request.</h1>");
            out.println("<div class='box'>");
            out.println("<p class='subtitle'>You may be contacted by admin if there are any further questions.</p>");
            out.println("<p>Otherwise, after you are approved, you will have support privileges.</p>");
            out.println("<br>");
            out.println("<div class='level'>");
            out.println("<a class='button is-primary is-outlined' href='UserView'>Back to your Profile Page</a>");
            out.println("<a class='button is-light' href='home.jsp'>Logout</a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("<br>");
            out.println("</section>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (EmailException ex) {
            Logger.getLogger(UserRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (EmailException ex) {
            Logger.getLogger(UserRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
