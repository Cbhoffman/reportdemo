/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserActions;

import Archive.AccessDB;
import SystemInteraction.Report;
import SystemInteraction.ReportManager;
import SystemInteraction.UserManager;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 * This servlet is used to submit a report into the database, display submission confirmation and email as required.
 * @author Caleb Hoffman
 */
@WebServlet(name = "SubmitReport", urlPatterns = {"/SubmitReport"})
public class SubmitReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
        
        if(!ServletFileUpload.isMultipartContent(request)){
            out.println("Nothing to Upload");
            return;
        }
        
        HashMap<String,String> inputs = new HashMap<String,String>();
        
        FileItemFactory itemfactory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(itemfactory);
        String filePath = null;
        String url = null;
        String originalFileName = null;
        
        // Current user id for the user that is logged in (retrieves from the previously created session).
        int user_id = (int) request.getSession(false).getAttribute("user_id");
        
        try{
            List<FileItem> items = upload.parseRequest(request);
            for (FileItem item : items){
                if (!item.isFormField())
                {
                    String uploadDir = "C:/Users/Caleb Hoffman/repos/reportdemo/DemoReportApp/web/images";
                    String fileName = FilenameUtils.getName(item.getName());
                    String seperator = "/";
                    url = "images";
                    originalFileName = fileName;
                    
                    // If the user does not submit an image it makes the filename "default.png"
                    if (fileName.isEmpty())
                    {
                        fileName = "default.png";
                    }

                    // File file = File.createTempFile("img",".png",uploadDir);
               
                    else {
                    // Prepends a random number to fileName in order to make it unique for storage and future selection.
                    long num = new Date().getTime();
                    fileName = num + "-" + fileName;
                    filePath = uploadDir + seperator + fileName;
                    File file = new File(filePath);
                    // Writes the filepath to the images folder...
                    item.write(file);
                    }
                    // The URL is stored in database.
                     url = url + seperator + fileName;

//                    out.println("File Saved Successfully");
//                    out.println("Filename: "+fileName);
//                    out.println("Filepath: "+filePath);
//                    out.println("URL: "+url);
                }
                else
                {
                    inputs.put(item.getFieldName(),item.getString());
                }           
            }            
        }
        catch(FileUploadException e){
            out.println("upload fail");
        }
        catch(Exception ex){
            out.println("can't save");
        }

        // After the image file is handled, the data is prepared for database entry.
        // The code below retrives the other form entry data from the hashmap above.
        String reportType = inputs.getOrDefault("reportType", "");
////        String email = inputs.getOrDefault("email", "").toLowerCase();
        String text = inputs.getOrDefault("text", "not provided");
        if("".equals(text))
        {
            text = "not provided";
        }
        String urgency = inputs.getOrDefault("urgency", "not provided");
        String geolocation = inputs.getOrDefault("location", "not provided");
        
////        // Checks to see if the email has been registered by a user before submitting the report.
////        if(LoginValidation.checkEmailAddress(email)){  
                
            // if valid then the report is created from the gathered data.
            Report newReport = new Report(); 
            newReport.setImg(url);
            newReport.setDetails(text);

//            newReport.setEmail(email);
            newReport.setUser_ID(user_id);
            newReport.setReportType(reportType);
            newReport.setGeolocation(geolocation);
            newReport.setUrgency(urgency);
            
            // The report is submitted into the database.
            ReportManager.insertReport(newReport);
            
            // HTML displays the report submission confirmation to the user.
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Submission Confirmation</title>");   
            out.println("<link rel='stylesheet' href='bulma.css'>");
            out.println("</head>");
            out.println("<body>");
//            Screen header = new Screen();        
//            header.makeHeader();     
            out.println("<section class='hero is-fullheight is-light is-bold'>");
            out.println("<br>");
            out.println("<div class='container has-text-centered'>");
            out.println("<div class='columns is-vcentered'> ");
            out.println("<div class='column is-6 is-offset-3'>");
          
            out.println("<h1 class='title'>Submission Confirmation</h1>");
            out.println("<div class='box'>");
            out.println("<h1 class='title'>Thank you for your report.</h1>");
            out.println("<br>");
            //out.println("<h2>You just submitted the following data:</h2>");  
    ////        out.println("<p>Email: "+email+"</p>");
            //out.println("<p>Details: "+text+"</p>");
            //out.println("<p>Report Type: "+reportType+"</p>");
            //out.println("<p>Urgency: "+urgency+"</p>");
            // Future work: possible inclusion of a map that displays the given geolocation?
            //out.println("<p>Location Coordinates: "+geolocation+"</p>");
            
    ////        out.println("<h3>You will receive an email with your report id number that you can use to track the status of your report.</h3>");
            out.println("<h3 class='subtitle'>You will be able to track the status of your report through your user profile page.</h3>");  
            out.println("<br>");
           // out.println("<p id='demo'></p>");
            out.println("<div class='level'>");
            out.println("<a class='button is-primary is-outlined' href='UserView'>Back to your Profile Page</a>");
            out.println("<a class='button is-default' href='home.jsp'>Logout</a>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("<br>");
            out.println("</section>");
            out.println("</body>");
            out.println("</html>");
            
            String authuser = "ueareport@gmail.com";
            String authpwd = "ueareporting";

            // Sending email to the support user...
            
            Email em = new SimpleEmail();
            em.setHostName("smtp.gmail.com");
            em.setSmtpPort(587);
            em.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
            em.setStartTLSEnabled(true);
            em.setFrom(authuser);
            em.setSubject("New Report Added");
            em.setMsg("A new report has been added by a user who said: "+text+ 
                    ". Please log in to see full details.");

            // Email delivery address depends upon the given Report Type.
            switch (reportType) 
            {
                case "security":
                    em.addTo("ueareport+security@gmail.com");
                    break;
                case "it":
                    em.addTo("ueareport+it@gmail.com");
                    break;
                case "mtFac":
                    em.addTo("ueareport+maintenance@gmail.com");
                    break;
                case "parking":
                    em.addTo("ueareport+parking@gmail.com");
                    break;
                case "union":
                    em.addTo("ueareport+studentunion@gmail.com");
                    break;
                default:
                    break;
            }
            em.send();
            
////            // Sending an email to the user who submitted their report
            Email userEmail = new SimpleEmail();
            userEmail.setHostName("smtp.gmail.com");
            userEmail.setSmtpPort(587);
            userEmail.setAuthenticator(new DefaultAuthenticator(authuser, authpwd));
            userEmail.setStartTLSEnabled(true);
            userEmail.setFrom(authuser);
            userEmail.setSubject("Thank you for submitting a report");
////            
////            int assignedId = ReportManager.getReportId(email, text);
////            
            userEmail.setMsg("Thank you for submitting a report. "+
                              "You can check the status of your report by logging in to the reporting portal. Thank you.");
////
////            // Below sends the email to the given address from the user.
            // Retrieves the current user email address from system.
            String email = UserManager.getUserEmail(user_id);
            userEmail.addTo(email); 
////            // Keep below commented out before everything is finalised.               
//            userEmail.send();
            
////            } else {  
////                // If the user supplies an email that is not registered within the system.
////                // They will be notified to supply the correct email address.
////                
////                // Future work: Ask user if they need to register and forward them appropriately.
////                out.print("Please enter correct email address");  
////                RequestDispatcher rd=request.getRequestDispatcher("newReport.jsp");  
////                rd.include(request,response);  
////            }  

    } catch ( EmailException ex ) {
                        Logger.getLogger(AccessDB.class.getName()).log(Level.SEVERE, null, ex);
                   }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SubmitReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SubmitReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}