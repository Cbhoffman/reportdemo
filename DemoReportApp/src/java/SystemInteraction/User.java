/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

/**
 * This is a class that creates user objects and basic methods for user objects.
 * @author Caleb Hoffman
 */
public class User {
    
    private String fName;
    private String lName;
    private String contact;
    private String email;
    
    private int user_id;
    private String role;
    
    private String user_login;
    private String password;
    private String reportType;

    public User() {};
  
    public String getFname() { return fName; }
    public String getLname() { return lName; }
    public String getContact() { return contact; }
    public String getEmail() { return email; }
    public int getUserId() { return user_id; }
    public String getRole() { return role; }
    
    public String getUser_Login() { return user_login; }
    public String getPassword() { return password; }
    public String getReportType() { return reportType; }

    public void setFname(String fName) { this.fName = fName; }
    public void setLname(String lName) { this.lName = lName; }
    public void setContact(String contact) { this.contact = contact; }
    public void setEmail(String email) { this.email = email; }
    public void setUserId(int user_id) { this.user_id = user_id; }
    public void setRole(String role) { this.role = role; }
    
    public void setUser_Login(String user_login) { this.user_login = user_login; }
    public void setPassword(String password) { this.password = password; }
    public void setReportType(String reportType) { this.reportType = reportType; }
    
    @Override
    public String toString()
    {        
        String str = lName + ", " + fName +
                ": " + contact + " " + email + "\n";
     
        return str;     
    }
    
//    @Override
//    public String toString()
//    {        
//        String str = lName + ", " + fName +
//                ": " + contact + " " + email + " " + user_id + " " + role + "\n";
//     
//        return str;     
//    }
    
    public User(String fName, String lName, String contact, String email, int user_id, String role) {

        this.fName = fName;
        this.lName = lName;
        this.contact = contact;
        this.email = email;   
        this.user_id = user_id;
        this.role = role;
  }

    // Initial user constructor, 
        public User(String fName, String lName, String contact, String email) {

        this.fName = fName;
        this.lName = lName;
        this.contact = contact;
        this.email = email;   
  }
    
    
//    
//    public static void main(String[] args){
//        User bob = new User("Bob","Newhart","07867584932","bnewhart@test.com");           
//        System.out.println(bob);
//    }
}
