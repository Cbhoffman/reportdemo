/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This class holds many methods that assist with database transactions.
 * @author Caleb Hoffman
 */
public class ReportManager {
    
    public static Connection getConnection()
    {  
        Connection con=null;  
        try{  
            Class.forName("org.postgresql.Driver");  
            con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "kc2285280");  
        }catch(Exception e){System.out.println(e);}  
        return con;  
    }  
    
    /**
     * This method is used for returning all ACTIVE reports (Status= 'Active' or 'In Work'). 
     * 
     * @param total the parameter that gets used for setting the LIMIT within this methods SQL statement
     * @param start the parameter that gets used for setting the OFFSET within this methods SQL statement
     * @param reportType the parameter that determines the Report Type
     * @return ArrayList of Reports
     */
    public static List<Report> getActiveRecords(int total,int start, String reportType)
    {  
        List<Report> list=new ArrayList<>();  
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("SELECT * FROM report LEFT JOIN userreg ON report.user_id = userreg.user_id WHERE reporttype = '"+reportType+"' AND status != 'Resolved' ORDER BY id LIMIT "+total+" offset "+start);      
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                Report e=new Report();  
                e.setId(rs.getInt("id"));  
                e.setTimeOccured(rs.getString("timeoccured"));  
                e.setFname(rs.getString("fName"));
                e.setLname(rs.getString("lName"));
                e.setContact(rs.getString("contact"));
                e.setEmail(rs.getString("email"));
                e.setReportType(rs.getString("reportType"));
                e.setDetails(rs.getString("details"));
                e.setImg(rs.getString("img"));
                e.setStatus(rs.getString("status"));
                e.setGeolocation(rs.getString("geolocation"));
                e.setUrgency(rs.getString("urgency"));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
    /**
     * This method is used for returning all reports (assists the search functionality). 
     * 
     * @param total the parameter that gets used for setting the LIMIT within this methods SQL statement
     * @param start the parameter that gets used for setting the OFFSET within this methods SQL statement
     * @param reportType the parameter that determines the Report Type
     * @param searchTerm the parameter that is the search term provided by the support user.
     * @return ArrayList of Reports
     */
    public static List<Report> getAllRecords(int total,int start, String reportType, String searchTerm)
    {  
        List<Report> list=new ArrayList<>();  
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("SELECT * FROM report LEFT JOIN userreg ON report.user_id = userreg.user_id WHERE reporttype = '"+reportType+"' AND details like '%_"+searchTerm+"%' ORDER BY status LIMIT "+total+" offset "+start);      
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                Report e=new Report();  
                e.setId(rs.getInt("id"));  
                e.setTimeOccured(rs.getString("timeoccured"));  
                e.setFname(rs.getString("fName"));
                e.setLname(rs.getString("lName"));
                e.setContact(rs.getString("contact"));
                e.setEmail(rs.getString("email"));
                e.setReportType(rs.getString("reportType"));
                e.setDetails(rs.getString("details"));
                e.setImg(rs.getString("img"));
                e.setStatus(rs.getString("status"));
                e.setGeolocation(rs.getString("geolocation"));
                e.setUrgency(rs.getString("urgency"));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
    /**
     * This method is used for returning all RESOLVED reports (Status= 'Resolved'). 
     * 
     * @param total the parameter that gets used for setting the LIMIT within this methods SQL statement
     * @param start the parameter that gets used for setting the OFFSET within this methods SQL statement
     * @param reportType the parameter that determines the Report Type
     * @return ArrayList of Reports
     */
    public static List<Report> getResolvedRecords(int total,int start, String reportType)
    {  
        List<Report> list=new ArrayList<>();  
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("SELECT * FROM report LEFT JOIN userreg ON report.user_id = userreg.user_id WHERE reporttype = '"+reportType+"'AND status = 'Resolved' ORDER BY id LIMIT "+total+" offset "+start);
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                Report e=new Report();  
                e.setId(rs.getInt("id"));  
               // e.setUser_ID(rs.getInt("user_id"));
                e.setTimeOccured(rs.getString("timeoccured"));  
                e.setFname(rs.getString("fName"));
                e.setLname(rs.getString("lName"));
                e.setContact(rs.getString("contact"));
                e.setEmail(rs.getString("email"));
                e.setReportType(rs.getString("reportType"));
                e.setDetails(rs.getString("details"));
                e.setImg(rs.getString("img"));
                e.setStatus(rs.getString("status"));
                e.setGeolocation(rs.getString("geolocation"));
                e.setUrgency(rs.getString("urgency"));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
    /**
     * This method is used for returning all reports of a given user. 
     * 
     * @param total the parameter that gets used for setting the LIMIT within this methods SQL statement
     * @param start the parameter that gets used for setting the OFFSET within this methods SQL statement
     * @param user_id the parameter that is a given user id number
     * @return ArrayList of Reports
     */
    public static List<Report> getUserReports(int total,int start, int user_id)
    {  
        List<Report> list=new ArrayList<>();  
        try{  
            Connection con=getConnection(); 
            PreparedStatement ps=con.prepareStatement("SELECT * FROM report LEFT JOIN userreg ON report.user_id = userreg.user_id WHERE report.user_id = "+user_id+" ORDER BY id LIMIT "+total+" offset "+start);
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                Report e=new Report();  
                e.setId(rs.getInt("id"));  
               // e.setUser_ID(rs.getInt("user_id"));
                e.setTimeOccured(rs.getString("timeoccured"));  
                e.setFname(rs.getString("fName"));
                e.setLname(rs.getString("lName"));
                e.setContact(rs.getString("contact"));
                e.setEmail(rs.getString("email"));
                e.setReportType(rs.getString("reportType"));
                e.setDetails(rs.getString("details"));
                e.setImg(rs.getString("img"));
                e.setStatus(rs.getString("status"));
                e.setGeolocation(rs.getString("geolocation"));
                e.setUrgency(rs.getString("urgency"));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
        /**
     * This method retrieves the number of records of a given user_id. 
     * @param user_id The parameter of a given user id number.
     * @return count of records
     */
    public static int getNumberofUserRecords(int user_id)
    {
        int count = 0;
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("select count(*) from report WHERE user_id = "+user_id+"");    
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                count = rs.getInt(1);     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return count;
    }
    
    /**
     * This method retrieves the number of ACTIVE records (Status= 'Active' or 'In Work') within the database. 
     * @param reportType The parameter that defines the Report Type.
     * @return int
     */
    public static int getNumberofActiveRecords(String reportType)
    {
        int count = 0;
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("select count(*) from report WHERE reporttype = '"+reportType+"' AND status != 'Resolved'");  
            //PreparedStatement ps=con.prepareStatement("select count(*) from report where reporttype = 'security'");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                count = rs.getInt(1);     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return count;
    }
    
    /**
     * This method retrieves the number of RESOLVED records (Status= 'Resolved') within the database. 
     * @param reportType The parameter that defines the Report Type.
     * @return int
     */
    public static int getNumberofResolvedRecords(String reportType)
    {
        int count = 0;
        try{  
            Connection con=getConnection();  
            //below did originally have limit as (start - 1)
            PreparedStatement ps=con.prepareStatement("select count(*) from report WHERE reporttype = '"+reportType+"' AND status = 'Resolved'");  
            //PreparedStatement ps=con.prepareStatement("select count(*) from report where reporttype = 'security'");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                count = rs.getInt(1);     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return count;
    }
    
    /**
     * This method retrieves a record within the database based on a given id.
     * @param id The parameter of a given Report Id number.
     * @return The Report containing the given id.
     * 
     * MIGHT NOT BE NEEDED ANYMORE...
     */
    public static Report getRecordById(String id)
    {
        Report searchedReport = new Report();
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from report where id = '"+id+"';");   
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                searchedReport.setId(rs.getInt("id")); 
                searchedReport.setTimeOccured(rs.getString("timeoccured"));
               
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}        
        return searchedReport;
    }
    
    /**
     * This method retrieves a Report Id number from the database.
     * @param email The parameter of a given email address.
     * @param details The parameter of the given report details.
     * @return The Report Id as an int.
     */
    public static int getReportId(String email, String details)
    {
        int assignedId = 0;
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select id from report where email = '"+email+"' and details ='"+details+"';");            
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                assignedId= rs.getInt("id"); 
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}       
        return assignedId;
    }
    
    
    /**
     * This method inserts a new report into the database.
     * @param newReport This parameter is a given Report object.
     */
    public static void insertReport(Report newReport)
    {
        try{  
            Connection con=getConnection();              
            PreparedStatement ps=con.prepareStatement("insert into report (user_id, details, reporttype, img, status, geolocation, urgency)"+
            " values ('"+newReport.getUser_ID()+"','"+newReport.getDetails()+"','"+newReport.getReportType()+"','"+newReport.getImg()+"','Active','"+newReport.getGeolocation()+"','"+newReport.getUrgency()+"');");       
            ps.executeUpdate();
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
    
    /**
     * This method is used to change the status of a report to 'In Work'.
     * @param id This parameter is a given Report Id number.
     */
    public static void setStatusInWork(String id)
    {
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("UPDATE report SET status = 'In Work' WHERE id = "+id+";");  
            ps.executeUpdate();
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
    
    /**
     * This method is used to change the status of a report to 'Resolved'.
     * @param id This parameter is a given Report Id number.
     */
    public static void setStatusResolved(String id){
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("UPDATE report SET status = 'Resolved' WHERE id = "+id+";");  
            ps.executeUpdate();
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
    
//    public static void main(String[] args){
//        String test = "09800765732";
//        if (Pattern.matches("[a-zA-Z]+", test) == false){
//            System.out.println("Contains letters");
//        }else{
//            System.out.println("Valid contact number");
//        }
//    }
}
