/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Caleb Hoffman
 */
public class UserManager {
    
        public static Connection getConnection()
    {  
        Connection con=null;  
        try{  
            Class.forName("org.postgresql.Driver");  
            con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
               "postgres", "kc2285280");  
        }catch(Exception e){System.out.println(e);}  
        return con;  
    }  
    
    /**
     * This method retrieves the total number of users (all roles within the system). 
     * @return int number of users
     */
    public static int getNumberofUsers()
    {
        int count = 0;
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select count(*) from userreg");    
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                count = rs.getInt(1);     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return count;
    }
    
    /**
     * This method retrieves the email of a given user id number. 
     * @param user_id given user id number
     * @return email of the user
     */
    public static String getUserEmail(int user_id)
    {
        String email = "";
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select email from userreg where user_id = "+user_id+"");    
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                email = rs.getString("email");     
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return email;
    }
    
    /**
     * This method is used for returning all users within the system (user, support and admin roles). 
     * 
     * @param total the parameter that gets used for setting the LIMIT within this methods SQL statement
     * @param start the parameter that gets used for setting the OFFSET within this methods SQL statement
     * @return ArrayList of Users
     */
    public static List<User> getUsers(int total,int start)
    {  
        List<User> list=new ArrayList<>();  
        try{  
            Connection con=getConnection();  
            
            PreparedStatement ps=con.prepareStatement("SELECT userreg.user_id, userreg.fname, userreg.lname, userreg.contact, userreg.email, userreg.role, login.user_login, login.password, login.reporttype FROM userreg Left JOIN login ON userreg.user_id = login.user_id ORDER BY lname LIMIT "+total+" offset "+start);
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                User e=new User();  
                e.setUserId(rs.getInt("user_id"));
                e.setUser_Login(rs.getString("user_login"));
                e.setFname(rs.getString("fName"));
                e.setLname(rs.getString("lName"));
                e.setContact(rs.getString("contact"));
                e.setEmail(rs.getString("email"));
                e.setReportType(rs.getString("reporttype"));
                e.setRole(rs.getString("role"));
                e.setPassword(rs.getString("password"));
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}  
        return list;  
    }  
    
    /**
     * This method returns user details.
     * @param user_id The parameter of a given user id number.
     * @return User details that include: First Name, Last Name, Contact Number and Email Address.
     */
    public static User getUser(int user_id)
    {
        User userReporting = new User();
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("select * from userreg where user_id = '"+user_id+"';");  
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
                userReporting.setFname(rs.getString("fName"));
                userReporting.setLname(rs.getString("lName"));
                userReporting.setContact(rs.getString("contact"));
                userReporting.setEmail(rs.getString("email"));
            }  
            con.close();  
        }catch(Exception e){System.out.println(e);}        
        return userReporting;
    }
    
    /**
     * This method is used to change the user role to 'support'.
     * @param user_id This parameter is a given User Id number.
     */
    public static void roleSupport(String user_id)
    {
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("UPDATE userreg SET role = 'support' WHERE user_id = "+user_id+";");  
            ps.executeUpdate();
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
    
    /**
     * This method is used for assigning a report area to a given user (support).
     * @param user_id This parameter is a given User Id number.
     * @param reportArea This parameter is a given report area.
     */
    public static void reportArea(String user_id, String reportArea)
    {
        try{  
            Connection con=getConnection();  
            PreparedStatement ps=con.prepareStatement("UPDATE login SET reporttype = '"+reportArea+"' WHERE user_id = "+user_id+";");  
            ps.executeUpdate();
            con.close();  
        }catch(Exception e){System.out.println(e);}  
    }
    
    /**
     * This method retrieves a User Id number from the database.
     * @param email The parameter of a given email address.
     * @return The User Id as an int.
     */
    public static int getUserId(String email) {
        int assignedId = 0;
        try {
            Connection con = ReportManager.getConnection();
            PreparedStatement ps = con.prepareStatement("select user_id from userreg where email = '" + email + "';");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                assignedId = rs.getInt("user_id");
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return assignedId;
    }

    /**
     * This method returns user details.
     * @param email The parameter of a given email address.
     * @return User details that include: First Name, Last Name, Contact Number and Email Address.
     */
    public static User getUser(String email) {
        User userReporting = new User();
        try {
            Connection con = ReportManager.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from user where email = '" + email + "';");
            //PreparedStatement ps=con.prepareStatement("select count(*) from report where reporttype = 'security'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                userReporting.setFname(rs.getString("fName"));
                userReporting.setLname(rs.getString("lName"));
                userReporting.setContact(rs.getString("contact"));
                userReporting.setEmail(rs.getString("email"));
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return userReporting;
    }

    /**
     * This method inserts a new user into the database.
     * @param newUser This parameter is a given User object.
     */
    public static void insertUser(User newUser) {
        try {
            Connection con = ReportManager.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into userreg (fname, lname, contact, email, role)" + "values ('" + newUser.getFname() + "','" + newUser.getLname() + "', '" + newUser.getContact() + "', '" + newUser.getEmail() + "', '" + newUser.getRole() + "');");
            ps.executeUpdate();
            int user_id = UserManager.getUserId(newUser.getEmail());
            PreparedStatement prs = con.prepareStatement("insert into login (user_id, user_login, password, reporttype)" + "values ('" + user_id + "','" + newUser.getUser_Login() + "', '" + newUser.getPassword() + "', 'none');");
            prs.executeUpdate();
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
