/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

/**
 * Report Class
 * @author Caleb Hoffman
 */

public class Report {
    
  private int id;
  private int user_id;
  private String timeOccured;  
  private String fName;
  private String lName;
  private String contact;
  private String email;
  private String reportType;
  private String details;
  private String img;
  private String status;
  private String geolocation;
  private String urgency;
  
  // Empty Report constructor.
  public Report() {};
  
  // creating a Report using the constructor
  
  /**
   * Constructor for creating a full report.
   * 
   * @param id determined within database upon insertion into database table.
   * @param user_id user id within the database
   * @param timeOccured determined within database upon insertion into database table.
   * @param fName first name of user
   * @param lName last name of user
   * @param contact user contact number
   * @param email user email
   * @param reportType type of report, ex. security
   * @param details given details of report
   * @param img filepath of image 
   * @param status active, in work or resolved
   * @param geolocation coordinates of the report location
   * @param urgency low, medium or high
   */
public Report(int id, String timeOccured, String fName, String lName, String contact, String email, String reportType, String details, String img, String status, String geolocation, String urgency) {
    this.id = id;
    this.timeOccured = timeOccured;
    this.fName = fName;
    this.lName = lName;
    this.contact = contact;
    this.email = email;
    this.reportType = reportType;
    this.details = details;
    this.img = img;
    this.status = status;
    this.geolocation = geolocation;
    this.urgency = urgency;
  }

    // Accessor methods

    public int getId() { return id; }
    public int getUser_ID() { return user_id; }
    public String getTimeOccured() { return timeOccured; }
    public String getFname() { return fName; }
    public String getLname() { return lName; }
    public String getContact() { return contact; }
    public String getEmail() { return email; }
    public String getReportType() { return reportType; }
    public String getDetails() { return details; }
    public String getImg() { return img; }
    public String getStatus() { return status; }
    public String getGeolocation() { return geolocation; }
    public String getUrgency() { return urgency; }

    // Mutator methods

    public void setId(int id) { this.id = id; }
    public void setUser_ID(int user_id) { this.user_id = user_id; }
    public void setTimeOccured(String timeOccured) { this.timeOccured = timeOccured; }
    public void setFname(String fName) { this.fName = fName; }
    public void setLname(String lName) { this.lName = lName; }            
    public void setContact(String contact) { this.contact = contact; }
    public void setEmail(String email) { this.email = email; }
    public void setReportType(String repType) { this.reportType = repType; }
    public void setDetails(String details) { this.details = details; }
    public void setImg(String img) { this.img = img; }
    public void setStatus(String status) { this.status = status; }
    public void setGeolocation(String geolocation) { this.geolocation = geolocation; }
    public void setUrgency(String urgency) { this.urgency = urgency; }

    // Returns the report information in a string format.
    @Override
    public String toString()
    {        
        String str = id + " : " + timeOccured + " " + lName + ", " + fName +
                " " + contact + " " + email + " " + reportType + " " + details +
                " " + img + " " + status + " " + geolocation + " " + urgency + "\n";
     
        return str;     
    }



public static void main(String[] args){
    Report test = ReportManager.getRecordById("2");
    
    String time = test.getTimeOccured();
    
    System.out.println(time.substring(0, 19));
    
    
}
}
