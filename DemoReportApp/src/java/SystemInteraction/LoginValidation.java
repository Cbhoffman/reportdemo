package SystemInteraction;


import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Caleb Hoffman
 */

// Created with help of following a tutorial at javatpoint.com, this class is called within the validation servlet...

public class LoginValidation {
    
    /**
     * Method that validates a given username and password with the database (simple validation). 
     * 
     * @param username the parameter providing the username.
     * @param password the parameter providing the password for the given username.
     * @return true or false.  
     */
    public static boolean validate(String username,String password){  

    boolean status=false;  
    
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement( 
        "select * from login where user_login=? and password=?");  
        ps.setString(1,username);  
        ps.setString(2,password);  

        ResultSet rs=ps.executeQuery();  
        status=rs.next();  

    } catch (Exception e){System.out.println(e);}  
        return status;  
    }  
    
/**
 * Method that selects the reportType assigned to the given username.
 * @param username the parameter providing the username.
 * @return String representing the reportType.
 */
 public static String reportTypeForUser(String username){  
    String reportType="";  
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement(  

        "select reporttype from login where user_login=? ");  
        ps.setString(1,username);  
       
        ResultSet rs=ps.executeQuery();  
        rs.next();
        reportType = rs.getString(1);

    } catch (Exception e){System.out.println(e);}  
        return reportType;  
    }
 
 /**
 * Method that retrieves the user role assigned to the given username.
 * @param username the parameter providing the username.
 * @return String representing the user role.
 */
 public static String getUserRole(String username){  
    String userRole="";  
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement("select role from userreg LEFT JOIN login ON userreg.user_id = login.user_id where user_login =?");  
        ps.setString(1,username);  
       
        ResultSet rs=ps.executeQuery();  
        rs.next();
        userRole = rs.getString("role");

    } catch (Exception e){System.out.println(e);}  
        return userRole;  
    }
 
 /**
 * Method that retrieves the stored password from the database for a given username.
 * @param username the parameter providing the username.
 * @return String representing the stored password hash.
 */
 public static String retrieveStoredPassword(String username){  
    String password="";  
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement("select password from login where user_login =?");  
        ps.setString(1,username);  
       
        ResultSet rs=ps.executeQuery();  
        rs.next();
        password = rs.getString("password");

    } catch (Exception e){System.out.println(e);}  
        return password;  
    }
 
 /**
 * Method that returns the user id assigned to the given username.
 * @param username the parameter providing the username.
 * @return int representing the user id.
 */
 public static int getCurrentUser(String username){  
    int userid = 0;  
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement(  
        "select user_id from login where user_login=? ");  
        ps.setString(1,username);  
       
        ResultSet rs=ps.executeQuery();  
        rs.next();
        userid = rs.getInt(1);
    } catch (Exception e){System.out.println(e);}  
        return userid;  
    }
 
 
 /**
  * Checks whether the given email currently exists in the database.
  * @param givenEmail the parameter providing the email address.
  * @return true or false.
  */
    public static boolean checkEmailAddress(String givenEmail){  
    boolean status=false;  
    try{  
        Class.forName("org.postgresql.Driver");  
        Connection con=DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                "postgres", "kc2285280");  

        PreparedStatement ps=con.prepareStatement(  
        "select * from userreg where email=?");  
        ps.setString(1,givenEmail);  
 
        ResultSet rs=ps.executeQuery();  
        status=rs.next(); 

    } catch (Exception e){System.out.println(e);}  
        return status;  
    } 
    
    // This method is from a websource...http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    // Author: Lokesh Gupta
    public static String generateStorngPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();
         
        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }
     
    // This method is from a websource...http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    // Author: Lokesh Gupta
    public static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
     
    // This method is from a websource...http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    // Author: Lokesh Gupta
    public static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }else{
            return hex;
        }
    }
    
    // This method is from a websource...http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    // Author: Lokesh Gupta
        public static boolean validatePassword(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);
         
        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();
         
        int diff = hash.length ^ testHash.length;
        for(int i = 0; i < hash.length && i < testHash.length; i++)
        {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }
        
    // This method is from a websource...http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    // Author: Lokesh Gupta
    public static byte[] fromHex(String hex) throws NoSuchAlgorithmException
    {
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++)
        {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
