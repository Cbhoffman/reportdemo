/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Caleb Hoffman
 */
@WebServlet(name = "AdminView", urlPatterns = {"/AdminView"})
public class AdminView extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {

        response.setContentType("text/html");  
        
        PrintWriter out=response.getWriter();  
          
        // Retrieves the number of the page that is clicked.
        String spageid=request.getParameter("page");
        
        // This initially sets the page value to one when support users log in.
        if (spageid == null)
        {
            spageid="1";
        }
        
        // Code below works out the correct amount of LIMIT and OFFSET for the SQL statement needed.
        int pageid=Integer.parseInt(spageid); 
        int resultsPerPage=5;  
        int offsetAmount;
        if(pageid==1)
        { 
            offsetAmount = 0;
        } else{  
            pageid=pageid-1;  
            offsetAmount=pageid*resultsPerPage;   
        }  
        
        int countOfRecords;
        List<User> list;
        
        // The code below gathers the system users. 
            countOfRecords = UserManager.getNumberofUsers();
            list=UserManager.getUsers(resultsPerPage, offsetAmount); 
        
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Previous Reports</title>");   
        out.println("<link rel='stylesheet' href='bulma.css'>");      
        out.println("<script src='reportjs.js' type='text/javascript'></script>");
        out.println("</head>");
        out.println("<body>");     
        
        out.println("<section class='hero is-fullheight is-light is-bold'>");
        out.println("<br>");
        
        out.println("<div class='container'>");
        out.println("<div class='columns is-vcentered'>");
        out.println("<div class='column is-14'>");
        
        out.println("<div class='level'>");
        out.println("<h1 class='title'>Registered Users</h1>");
        out.println("<form action='Logout'>");       
        out.println("<input type='submit' class='button is-light' value='Logout' />");
        out.println("</form>");
        out.println("</div>");

        out.println("<div class='box'>");
        out.println("<form>");
       
        out.println("<table class='table box'>");  
        //out.println("<thead><tr><th></th><th>Id</th><th>Time Occured</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Report Type</th><th>Details</th><th>Image</th><th>Status</th><th>Geolocation</th><th>Urgency</th></tr></thead>");  
        out.println("<thead><tr><th>Select</th><th>User ID</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Role</th><th>Report Area</th></tr></thead>"); 
        out.println("<tfoot><tr><th>Select</th><th>User ID</th><th>First Name</th><th>Last Name</th><th>Contact</th><th>Email</th><th>Role</th><th>Report Area</th></tr></tfoot>"); 
        out.println("<tbody>");
        for(User e:list)
        {  
            //out.println("<tr><td><input type='checkbox' name='reports' value='"+e.getId()+"'></td><td>"+e.getId()+"</td><td>"+e.getTimeOccured()+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getReportType()+"</td><td>"+e.getDetails()+"</td><td><img class=rpImg style='width:200px;height:200px;' src="+e.getImg()+"></td><td>"+e.getStatus()+"</td><td><button class='locButton' id='location"+e.getId()+"' type='button' onclick='mapDisplay()' value='"+e.getGeolocation()+"' />View Location</button></td><td>"+e.getUrgency()+"</td></tr>");     
            out.println("<tr><td><input type='checkbox'  name='users' value='"+e.getUserId()+"'></td><td>"+e.getUserId()+"</td><td>"+e.getFname()+"</td><td>"+e.getLname()+"</td><td>"+e.getContact()+"</td><td>"+e.getEmail()+"</td><td>"+e.getRole()+"</td><td>"+e.getReportType()+"</td></tr>");    
        }  

        out.println("</tbody>");
        out.println("</table>");  
        
        out.println("<label class='label'>Report Area</label>");
       
        List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
            for(ReportTypes.Type type : types)
            { 
                String value = type.id;
                String insert = type.name;

            //Displays the different report types (from reporttypes class) within the form.
            out.println("<input type='radio' name='reportType' value='"+value+"' /> "+insert+"<br />");            
            }
      
        out.println("<br>");
        out.println("<input type='submit' class='button is-primary' formaction='RoleChangeSupport' value='Change Role to Support' />");
        out.println("</form>");           
        out.println("<br>");
        int originalPage = Integer.parseInt(spageid);
        
        // Code below creates the paging for viewing the users within the system.
        int totalPages = countOfRecords/resultsPerPage + 1;
      
      out.println("<nav class='pagination'>") ;
      if (originalPage == 1){
          out.println("<a class='pagination-previous' disabled>Previous</a>");
      } else {
          out.println("<a class='pagination-previous' href='AdminView?page="+(originalPage-1)+"'>Previous</a>");
      }
      if (originalPage == totalPages){
          out.println("<a class='pagination-next' disabled>Next page</a>");
      } else {
          out.println("<a class='pagination-next' href='AdminView?page="+(originalPage+1)+"'>Next page</a>");
      }

      out.println("<ul class='pagination-list'>");
      
        for (int i=1; i < totalPages +1; i++)
            {
                out.println("<li>");
                if (originalPage == i){
                    out.println("<a class=\"pagination-link is-current\" href='AdminView?page="+i+"'>"+i+"</a> ");  
                } else {
                    out.println("<a class=\"pagination-link\" href='AdminView?page="+i+"'>"+i+"</a> ");  
                }
                out.println("</li>");
            }
        out.println("</ul>");
        out.println("</nav>");
        out.println("<br>"); 
        out.println("</div>");
        out.println("</div>");
        out.println("</div>");
        out.println("</div>");
        out.println("<br>");
        out.println("</section>");
        out.println("</body>");
        out.println("</html>");

        out.close();  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
