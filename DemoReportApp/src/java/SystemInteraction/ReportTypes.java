/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Caleb Hoffman
 */
public class ReportTypes {
    public class Type
    {
        public Type(String i, String n) { id = i; name = n; }
        public String id;
        public String name;
    }
    
    private List<Type> types;
    
    public ReportTypes()
    {
        types = new ArrayList<Type>();
        
        types.add(new Type("security","Security"));
        types.add(new Type("it","IT Issues"));
        types.add(new Type("mtFac","Maintenance and Facilities"));
        types.add(new Type("parking","Parking and Transportation"));
        types.add(new Type("union","Student Union"));
    }
    
    public List<Type> getTypes() { return types; }
  
}
