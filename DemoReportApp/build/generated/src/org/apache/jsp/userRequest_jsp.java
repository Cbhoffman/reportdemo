package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import SystemInteraction.ReportTypes;
import java.util.List;

public final class userRequest_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>Support Team Request</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <section class=\"hero is-fullheight is-light is-bold\">\n");
      out.write("        <br>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"columns is-vcentered\">\n");
      out.write("                <div class=\"column is-two-thirds is-offset-2\">\n");
      out.write("        \n");
      out.write("        <div class=\"level\">            \n");
      out.write("            <h1 class=\"title\">Be part of the Support Team</h1>\n");
      out.write("            <form action='Logout'>      \n");
      out.write("                <input type='submit' class='button is-light' value='Logout' />\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("        <div class=\"box\">\n");
      out.write("        \n");
      out.write("        <p class=\"subtitle\">This system can always use more workers to resolve issues around the UEA campus.</p>\n");
      out.write("        <p>If you are interested in becoming someone that can contribute to resolving different issues, please fill out the short form below, which will be forwarded to admin for approval.</p>\n");
      out.write("        <br>\n");
      out.write("         <label class=\"label\">Please select the support area that you would like to join.</label>\n");
      out.write("         <form name= 'requestForm' action='UserRequest' method='post' onSubmit=\"return validate()\" >\n");
      out.write("                ");

                  List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                  for(ReportTypes.Type type : types)
                  { 
                      String value = type.id;
                      String insert = type.name;
                      
               // out.print(value + " "+insert);
                out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                
                      //type.id;
                      //type.name;
                  }
                
      out.write("\n");
      out.write("                <br />\n");
      out.write("               \n");
      out.write("\n");
      out.write("                <label class=\"label\"> A brief description of why you should be considered or any relevant qualifications for the chosen report area. </label>\n");
      out.write("               \n");
      out.write("                <textarea class=\"textarea\" cols=\"30\" rows=\"4\" name=\"requestDetails\"></textarea><br /> </p>\n");
      out.write("                \n");
      out.write("                <div class=\"level\">\n");
      out.write("                <input type=\"submit\" class=\"button is-primary\" value=\"Submit Your Request to Admin\"/>\n");
      out.write("                <a class='button is-primary is-outlined' href='UserView?page=1'>Return to Profile Page</a><br /><br />\n");
      out.write("               \n");
      out.write("                </div>\n");
      out.write("    </form>\n");
      out.write("                </div>\n");
      out.write("                <br>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("                </div>\n");
      out.write("                </div>\n");
      out.write("                </div>\n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
