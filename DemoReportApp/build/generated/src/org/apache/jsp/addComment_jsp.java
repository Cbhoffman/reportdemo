package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;

public final class addComment_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!-- NOT NEEDED... -->\n");
      out.write("\n");
//@page import="uk.ac.uea.cmp.javaweb.Comments.Comment"
      out.write('\n');
//@page import="uk.ac.uea.cmp.javaweb.Comments"
      out.write('\n');
//@page contentType="text/html" pageEncoding="UTF-8"
      out.write("\n");
      out.write("<!--\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("-->\n");
      out.write("    ");

        //Comments c = new Comments();
        
        // Is there a new comment to add at this point? If so add it now.
        // n.b. we could also do this on a seperate page and redirect back
        // such as addComment.jsp but here for simplicity EVEN IF NOT GOOD
        // DESIGN PRACTICE!
        
        //String name = request.getParameter("name");
        //String text = request.getParameter("text");
        
        //if (name != null && text != null)
        //{
        //    c.Add(name, text);
        //}
        
        // Now read and print the comments
        //List<Comment> comments = c.Get();
        //for(Comment comment: comments)          
        //{
        //    out.print("<b>By: "+comment.name+", at "+comment.posted+"</b><br /><br />");
        //    out.print(comment.text);
        //    out.print("<br /><br /><hr />");
        //}

/*
if(request.getParameter("btnSubmit")!=null) //btnSubmit is the name of your button, not id of that button.
{
     comments.clear();
}
*/
String email = "test@test.com";
String text = "Some random report being submitted into the system.";
String reportType = "security";
String urgency = "low";
String geolocation = "coordinates";

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Submission Confirmation</title>");   
            out.println("<link rel='stylesheet' href='bulma.css'>");
            out.println("</head>");
            out.println("<body>");  
            out.println("<div class='container content is-fluid'>");
            out.println("<div class='columns is-centered'> ");
            out.println("<div class='column is-two-thirds'>");
            out.println("<h1>Thank you for your report.</h1>");
            out.println("<h2>You just submitted the following data:</h2>");  
            out.println("<p>Email: "+email+"</p>");
            out.println("<p>Details: "+text+"</p>");
            out.println("<p>Report Type: "+reportType+"</p>");
            out.println("<p>Urgency: "+urgency+"</p>");
            // Future work: possible inclusion of a map that displays the given geolocation?
            out.println("<p>Location Coordinates: "+geolocation+"</p>");
            
            out.println("<h3>You will receive an email with your report id number that you can use to track the status of your report.</h3>");  
            
            out.println("<p id='demo'></p>");
            
            out.println("<a href='home.jsp'>Back to the Home Page</a><br /><br />");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");



       
    
      out.write("\n");
      out.write("<!--    \n");
      out.write("    <br />\n");
      out.write("        <a href=\"createcomment.jsp\">Report Again</a>\n");
      out.write("\n");
      out.write("        <input type=\"submit\" id=\"btnSubmit\" name=\"btnSubmit\" value=\"Clear Report History\"/>\n");
      out.write("        \n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("-->\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
