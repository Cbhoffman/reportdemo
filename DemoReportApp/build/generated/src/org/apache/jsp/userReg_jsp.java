package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class userReg_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>New User Registration</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("       <section class=\"hero is-fullheight is-light is-bold\"> \n");
      out.write("           <br>\n");
      out.write("       <div class=\"container\">         \n");
      out.write("           <div class=\"columns is-vcentered\"> \n");
      out.write("                <div class=\"column is-4 is-offset-4\">\n");
      out.write("                <h1 class=\"title\">Please Register Here</h1>\n");
      out.write("            <form name= 'userReg' action='UserReg' method='post'> <!-- onSubmit=\"return validate()\" -->\n");
      out.write("                <div class=\"box\">\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">First Name</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"fName\" placeholder=\"First Name\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Last Name</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"lName\" placeholder=\"Last Name\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Contact</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"contact\" placeholder=\"Contact\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Email</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"text\" name=\"email\" placeholder=\"Email\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("                                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Username</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"text\" name=\"username\" placeholder=\"Username\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Password</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"password\" name=\"password\" placeholder=\"Password\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("             <!--   <p> First Name: <input type=\"text\" name=\"fName\" placeholder=\"First name\" /> </p> \n");
      out.write("                <p> Last Name: <input type=\"text\" name=\"lName\" placeholder=\"Last name\" /> </p> \n");
      out.write("                <p> Contact Number: <input type=\"text\" name=\"contact\" /> </p>\n");
      out.write("                <p> Email: <input type=\"text\" name=\"email\" /> </p> -->\n");
      out.write("                <br />\n");
      out.write("\n");
      out.write("                <input class=\"button is-primary\" type=\"submit\" value=\"Complete Registration\"/>\n");
      out.write("                <a class=\"button is-default\" href=\"home.jsp\">Cancel</a>\n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("            </form>\n");
      out.write("        \n");
      out.write("        <br />        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                </div>\n");
      out.write("           </div>  \n");
      out.write("            \n");
      out.write("\n");
      out.write("           \n");
      out.write("        </div>\n");
      out.write("        <br>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
