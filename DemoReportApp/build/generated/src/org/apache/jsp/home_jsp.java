package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!--The design of the home page is based on a template from https://dansup.github.io/bulma-templates/templates/cover.html-->\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>Reporting Portal Home</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("      \n");
      out.write("    <section class=\"hero is-fullheight is-default is-bold\">\n");
      out.write("    <!-- Hero header: will stick at the top -->\n");
      out.write("    <div class=\"hero-head\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("        <nav class=\"nav has-shadow\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("             <!--   <div class=\"nav-left\">\n");
      out.write("                    <h1 class=\"nav-item\">\n");
      out.write("                        UEA Reporting Portal  \n");
      out.write("                    </h1>\n");
      out.write("                </div>-->\n");
      out.write("                <span class=\"nav-toggle\">\n");
      out.write("                  <span></span>\n");
      out.write("                  <span></span>\n");
      out.write("                  <span></span>\n");
      out.write("                </span>\n");
      out.write("                <div class=\"nav-right nav-menu\">\n");
      out.write("                  <a href=\"home.jsp\" class=\"nav-item is-tab is-active\">\n");
      out.write("                    Home\n");
      out.write("                  </a>\n");
      out.write("                  <a href=\"faq.jsp\" class=\"nav-item is-tab\">\n");
      out.write("                    FAQ\n");
      out.write("                  </a>\n");
      out.write("                    <span class=\"nav-item\">\n");
      out.write("                      <a href=\"userReg.jsp\" class=\"button is-info\">\n");
      out.write("                        Register\n");
      out.write("                      </a>\n");
      out.write("                    </span>\n");
      out.write("                    <span class=\"nav-item\">\n");
      out.write("                      <a href=\"SupportLogin.jsp\" class=\"button\">\n");
      out.write("                        Log in\n");
      out.write("                      </a>\n");
      out.write("                    </span>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        </div>\n");
      out.write("    </div>    \n");
      out.write("      \n");
      out.write("  <!-- Hero content: will be in the middle -->\n");
      out.write("  <div class=\"hero-body\">\n");
      out.write("    <div class=\"container has-text-centered\">\n");
      out.write("        <div class=\"columns is-vcentered\">\n");
      out.write("            <div class=\"column is-5\">\n");
      out.write("                <figure class=\"image is-3by4\">\n");
      out.write("                    <img src=\"images/UEA_LOGO.png\" alt=\"UEA Logo\" style=\"width:600px;height:270px;\">\n");
      out.write("                </figure>\n");
      out.write("            </div>\n");
      out.write("        <div class=\"column is-6 is-offset-1\">\n");
      out.write("      <h1 class=\"title is-2\">\n");
      out.write("        UEA Reporting Portal  \n");
      out.write("      </h1>\n");
      out.write("      <h2 class=\"subtitle is-4\">\n");
      out.write("        MSc Computing Science Dissertation Project\n");
      out.write("      </h2>\n");
      out.write("      <br>\n");
      out.write("      <p class=\"has-text-centered\">\n");
      out.write("          <a href=\"faq.jsp\" class=\"button is-large\">\n");
      out.write("              Learn More\n");
      out.write("          </a>\n");
      out.write("      </p>\n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("\n");
      out.write("    <!-- Hero footer: will stick at the bottom -->\n");
      out.write("    <div class=\"hero-foot\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"tabs is-centered\">\n");
      out.write("                <ul>\n");
      out.write("                <li class=\"is-active\"><a href=\"http://www.uea.ac.uk\">Image via UEA</a></li>\n");
      out.write("                <li><a>Project by Caleb Hoffman 2017</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("    </section>\n");
      out.write("        \n");
      out.write("      \n");
      out.write("        \n");
      out.write("\n");
      out.write("        \n");
      out.write("        <script async type=\"text/javascript\" src=\"bulma.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
