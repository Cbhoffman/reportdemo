package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class faq_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>FAQ Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("        <nav class=\"nav has-shadow\">\n");
      out.write("            <div class=\"container\">\n");
      out.write("            <!--    <div class=\"nav-left\">\n");
      out.write("                    <h1 class=\"nav-item\">\n");
      out.write("                       UEA Reporting Portal  \n");
      out.write("                    </h1>\n");
      out.write("                </div>-->\n");
      out.write("                <span class=\"nav-toggle\">\n");
      out.write("                  <span></span>\n");
      out.write("                  <span></span>\n");
      out.write("                  <span></span>\n");
      out.write("                </span>\n");
      out.write("                <div class=\"nav-right nav-menu\">\n");
      out.write("                  <a href=\"home.jsp\" class=\"nav-item is-tab\">\n");
      out.write("                    Home\n");
      out.write("                  </a>\n");
      out.write("                  <a href=\"faq.jsp\" class=\"nav-item is-tab is-active\">\n");
      out.write("                    FAQ\n");
      out.write("                  </a>\n");
      out.write("                    <span class=\"nav-item\">\n");
      out.write("                      <a href=\"userReg.jsp\" class=\"button is-info\">\n");
      out.write("                        Register\n");
      out.write("                      </a>\n");
      out.write("                    </span>\n");
      out.write("                    <span class=\"nav-item\">\n");
      out.write("                      <a href=\"SupportLogin.jsp\" class=\"button\">\n");
      out.write("                        Log in\n");
      out.write("                      </a>\n");
      out.write("                    </span>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <section class='hero is-primary is-medium'> \n");
      out.write("            <div class='hero-body'>\n");
      out.write("                <div class='container has-text-centered'>\n");
      out.write("                    <h1 class='title is-2'>\n");
      out.write("                        Frequently Asked Questions\n");
      out.write("                    </h1>\n");
      out.write("                </div>    \n");
      out.write("            </div>\n");
      out.write("        </section>\n");
      out.write("                    \n");
      out.write("        <section class='section blog'> \n");
      out.write("            <div class='container'>\n");
      out.write("                <div class='columns '>\n");
      out.write("                    <div class='column is-8 is-offset-2'>\n");
      out.write("                        <div class='content blog-post section'>\n");
      out.write("            \n");
      out.write("        <h1>What is the UEA Reporting Portal?</h1>\n");
      out.write("        <p>A single access point to report information around campus. If you have something to say but don't know where to say it, you have come to the right place.</p>\n");
      out.write("        <h1>Why is this needed?</h1>\n");
      out.write("        <p>Currently, there are different types of issues around campus someone might come across in their time at UEA, whether you are a student or staff member. However, in order to report an issue, you have to find the right people, office or location that is relevant to your specific problem in order to inform those who would resolve your particular issue. </p>\n");
      out.write("        <p> With this problem in mind, the UEA Reporting Portal was created to become a place where you can report a range of different issues from one simple location.</p>\n");
      out.write("        <h1>I am a first time user. How do you use this portal?</h1>\n");
      out.write("        <ol>\n");
      out.write("            <li><a href='userReg.jsp'>Register your details</a> in order create an account in the system.</li>\n");
      out.write("            <li>Once you have registered, you can then click 'Submit a Report'.</li>\n");
      out.write("            <li>Fill in the form and press 'Submit'</li>\n");
      out.write("        </ol>\n");
      out.write("        <p>After registering, you can log into the system with your personal username and password in order to view your report status, or to submit additional reports.</p>\n");
      out.write("        <h1>What happens when a report is submitted?</h1>\n");
      out.write("        <p>The information will be forwarded to the appropriate member of the support team. With the information you provide, these individuals will be able to resolve your issue in a suitable manner.</p>\n");
      out.write("        \n");
      out.write("        <h1>Can I track the status of my submitted report?</h1>\n");
      out.write("        <p>Yes, you can view the status of your report by selecting the 'View Your Previously Submitted Reports' button on your profile page.</p>\n");
      out.write("        \n");
      out.write("        <h1>How can I help resolve issues?</h1>\n");
      out.write("        <p>If you would like to be involved in resolving issues around campus you can request to become part of the support team. This can be achieved through selecting the 'Join the Support Team' button on your personal profile page. </p>\n");
      out.write("        </div>\n");
      out.write("        </div>\n");
      out.write("        </div>\n");
      out.write("               </div>\n");
      out.write("        </section>\n");
      out.write("                    \n");
      out.write("            <div class=\"hero-foot\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"tabs is-centered\">\n");
      out.write("                <ul>\n");
      out.write("                <li class=\"is-active\"><a href=\"http://www.uea.ac.uk\">Image via UEA</a></li>\n");
      out.write("                <li><a>Project by Caleb Hoffman 2017</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>            \n");
      out.write("                \n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <script async type=\"text/javascript\" src=\"bulma.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
