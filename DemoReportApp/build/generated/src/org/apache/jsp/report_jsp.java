package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import AccessDB.ReportTypes;
import uk.ac.uea.cmp.javaweb.Reports.Report;
import java.util.List;
import uk.ac.uea.cmp.javaweb.Reports;

public final class report_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <script src=\"reportjs.js\" type=\"text/javascript\"></script>\n");
      out.write("        \n");
      out.write("        <title>Reporting Home page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <h1>Report your complaint here.</h1>\n");
      out.write("            \n");
      out.write("                <h2>Please select the area that applies the most to your report.</h2>\n");
      out.write("               <!--\n");
      out.write("                <input type=\"radio\" name=\"reportType\" value=\"security\">Security<br>\n");
      out.write("                <input type=\"radio\" name=\"reportType\" value=\"it\">IT<br>\n");
      out.write("                <input type=\"radio\" name=\"reportType\" value=\"mtFac\">Maintenance and Facilities<br>\n");
      out.write("                <input type=\"radio\" name=\"reportType\" value=\"parking\">Parking and Transportation\n");
      out.write("               \n");
      out.write("                <br />\n");
      out.write("                <br />\n");
      out.write("               -->\n");
      out.write("               <form name= 'report' action='AccessDB' method='post' onSubmit=\"return validate()\" >\n");
      out.write("                ");

                  List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                  for(ReportTypes.Type type : types)
                  { 
                      String value = type.id;
                      String insert = type.name;
                      
               // out.print(value + " "+insert);
                out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                
                      //type.id;
                      //type.name;
                  }
                
      out.write("\n");
      out.write("                <br />\n");
      out.write("                <br />\n");
      out.write("                \n");
      out.write("                Your Name: <input type=\"text\" name=\"name\" placeholder=\"Your name\" /><br />\n");
      out.write("                Contact Number: <input type=\"text\" name=\"contact\" /><br />\n");
      out.write("     <!--       Address: <input type=\"text\" name=\"address\" /><br />\n");
      out.write("                Postcode: <input type=\"text\" name=\"postcode\" /><br />  -->\n");
      out.write("                Email: <input type=\"text\" name=\"email\" /><br />\n");
      out.write("                Report Details: <textarea cols=\"30\" rows=\"4\" name=\"text\"></textarea><br />\n");
      out.write("                <br />\n");
      out.write("                \n");
      out.write("                <input type=\"submit\" value=\"Submit Your Report\"/>\n");
      out.write("    </form>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("                <h2>Search for additional reports.</h2>\n");
      out.write("                <form name= 'searchID' action='Search' method='post' >\n");
      out.write("                Search By Report ID: <input type='text' name='search' placeholder='ID' /><br />\n");
      out.write("                <br />\n");
      out.write("                <input type='submit' value='Search' />\n");
      out.write("                </form>\n");
      out.write("           \n");
      out.write("        \n");
      out.write("        \n");
      out.write("        \n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
