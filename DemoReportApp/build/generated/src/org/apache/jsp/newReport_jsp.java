package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import SystemInteraction.ReportTypes;
import java.util.List;

public final class newReport_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--Landing page for users wishing to submit a report.-->\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>Submitting Report</title>\n");
      out.write("     \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <section class='hero is-fullheight is-light is-bold'>\n");
      out.write("            <br>\n");
      out.write("        <div class=\"container\">\n");
      out.write("             <div class=\"columns is-vcentered\"> \n");
      out.write("                 <div class=\"column is-8 is-offset-2\">\n");
      out.write("                     \n");
      out.write("        <div class=\"level\">\n");
      out.write("            <h1 class=\"title\">Submit your report here.</h1>\n");
      out.write("            <form action='Logout'>   \n");
      out.write("            <input type='submit' class='button is-light' value='Logout' />\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("    \n");
      out.write("        <form name= 'report' action='SubmitReport' method='post' enctype=\"multipart/form-data\" onSubmit=\"return validate()\" >\n");
      out.write("           <div class='box'>\n");
      out.write("            <div class='field'>\n");
      out.write("                <div class='control'>\n");
      out.write("            <label class=\"label\">Please select the area that applies the most to your report:</label>\n");
      out.write("            ");

              List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
              for(ReportTypes.Type type : types)
              { 
                  String value = type.id;
                  String insert = type.name;

                  //Displays the different report types (from reporttypes class) within the form.
                  out.println("<input type='radio' name='reportType' value='"+value+"' /> "+insert+"<br />");
              }
            
      out.write("\n");
      out.write("            \n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        <br>    \n");
      out.write("            \n");
      out.write("         <!--\n");
      out.write("            First Name: <input type=\"text\" name=\"fName\" placeholder=\"First name\" /><br />\n");
      out.write("            Last Name: <input type=\"text\" name=\"lName\" placeholder=\"Last name\" /><br /> \n");
      out.write("            Contact Number: <input type=\"text\" name=\"contact\" /><br /> -->\n");
      out.write("    <!--        <p> Email: <input type=\"text\" name=\"email\" /> </p>   -->\n");
      out.write("            \n");
      out.write("        <div class='field'>\n");
      out.write("            <label class=\"label\"> Level of Urgency: </label>\n");
      out.write("            <div class='control'>\n");
      out.write("                <div class='select'>\n");
      out.write("                <select name=\"urgency\">\n");
      out.write("          <!--          <option value=\"default\" selected=\"selected\"></option> -->\n");
      out.write("                    <option value=\"low\" selected=\"selected\"> Low</option>\n");
      out.write("                    <option value=\"medium\"> Medium</option>\n");
      out.write("                    <option value=\"high\"> High</option>\n");
      out.write("                </select>\n");
      out.write("                </div>\n");
      out.write("            </div>    \n");
      out.write("        </div>\n");
      out.write("          \n");
      out.write("            <br>\n");
      out.write("                        \n");
      out.write("        <div class='field'>\n");
      out.write("            <label class=\"label\"> Insert Photo: </label>\n");
      out.write("            <div class='control'>\n");
      out.write("            <!-- Insert Photo: <input type=\"file\" name=\"file\" size=\"50\" capture=\"camera\" accept=\"image/*\"/><br /><br /> -->\n");
      out.write("            <input type=\"file\" name=\"file\" size=\"40\" accept=\"image/*\">  \n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("        <div class='field'>\n");
      out.write("            <label class=\"label\"> Provide Geolocation Information </label>\n");
      out.write("            <div class='control'>\n");
      out.write("            <input type=\"checkbox\" id=\"location\" name=\"location\" value=\"geolocation\" onclick=\"getLocation()\">  (Only if you are at the location where your issue has occured.)\n");
      out.write("            </div>\n");
      out.write("        </div>   \n");
      out.write("            \n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("        <div class='field'>\n");
      out.write("            <label class=\"label\"> Report Details: </label>\n");
      out.write("            <div class='control'>\n");
      out.write("            <textarea class=\"textarea\" cols=\"30\" rows=\"4\" name=\"text\"></textarea> \n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("            <br>\n");
      out.write("            \n");
      out.write("            <div class=\"level\">\n");
      out.write("   \n");
      out.write("            <input type=\"submit\" class=\"button is-primary\" value=\"Submit Your Report\"/>    \n");
      out.write("    \n");
      out.write("            <a class='button is-primary is-outlined' href='UserView'>Back to your Profile Page</a>    \n");
      out.write("                             \n");
      out.write("\n");
      out.write("            \n");
      out.write("            </div>\n");
      out.write("           </div>\n");
      out.write("        </form>\n");
      out.write("\n");
      out.write("\n");
      out.write("            \n");
      out.write("            <p id=\"demo\">\n");
      out.write("\n");
      out.write("            <script type=\"text/javascript\" src=\"reportjs.js\"></script>  \n");
      out.write("                 </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("            <br>\n");
      out.write("        </section>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
