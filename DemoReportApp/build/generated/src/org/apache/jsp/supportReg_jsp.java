package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import SystemInteraction.ReportTypes;
import java.util.List;

public final class supportReg_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"bulma.css\">\n");
      out.write("        <title>Support Registration</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       <div class=\"container content is-fluid\">  \n");
      out.write("            \n");
      out.write("           <div class=\"columns is-centered\"> \n");
      out.write("                <div class=\"column is-two-thirds\">\n");
      out.write("                                  \n");
      out.write("        <h1>Please Register Here:</h1>\n");
      out.write("            <form name= 'supportReg' action='SupportReg' method='post'> <!-- onSubmit=\"return validate()\" -->\n");
      out.write("                <div class=\"box\">\n");
      out.write("                <label class=\"label\">Which section are you registering for?</label>\n");
      out.write("                ");

                    List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                    for(ReportTypes.Type type : types)
                    { 
                        String value = type.id;
                        String insert = type.name;

                        //Displays the different report types (from reporttypes class) within the form.
                        out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                    }
                
      out.write("   \n");
      out.write("                <br>  \n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">First Name</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"fName\" placeholder=\"First Name\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Last Name</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"lName\" placeholder=\"Last Name\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Contact</label>\n");
      out.write("                        <p class=\"control\">\n");
      out.write("                            <input class=\"input\" type=\"text\" name=\"contact\" placeholder=\"Contact\">\n");
      out.write("                        </p>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Email</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"text\" name=\"email\" placeholder=\"Email input\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Username</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"text\" name=\"username\" placeholder=\"Username\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("                <div class=\"field\">\n");
      out.write("                    <label class=\"label\">Password</label>\n");
      out.write("                    <p class=\"control\">\n");
      out.write("                      <input class=\"input\" type=\"password\" name=\"password\" placeholder=\"Password\">\n");
      out.write("                    </p>      \n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <br />\n");
      out.write("\n");
      out.write("                <input class=\"button is-primary\" type=\"submit\" value=\"Complete Registration\"/>\n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("            </form>\n");
      out.write("        \n");
      out.write("        <br />        \n");
      out.write("        \n");
      out.write("        <a href=\"home.jsp\">Back to Home Page</a>               \n");
      out.write("                    \n");
      out.write("                </div>\n");
      out.write("           </div>  \n");
      out.write(" \n");
      out.write("        </div>\n");
      out.write("        <br>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
