<%-- 
    Document   : home
    Created on : 20-Jun-2017, 13:52:17
    Author     : Caleb Hoffman
--%>

<!--The design of the home page is based on a template from https://dansup.github.io/bulma-templates/templates/cover.html-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>Reporting Portal Home</title>
    </head>
    <body>

      
    <section class="hero is-fullheight is-default is-bold">
    <!-- Hero header: will stick at the top -->
    <div class="hero-head">
        <div class="container">
        <nav class="nav has-shadow">
            <div class="container">
             <!--   <div class="nav-left">
                    <h1 class="nav-item">
                        UEA Reporting Portal  
                    </h1>
                </div>-->
                <span class="nav-toggle">
                  <span></span>
                  <span></span>
                  <span></span>
                </span>
                <div class="nav-right nav-menu">
                  <a href="home.jsp" class="nav-item is-tab is-active">
                    Home
                  </a>
                  <a href="faq.jsp" class="nav-item is-tab">
                    FAQ
                  </a>
                    <span class="nav-item">
                      <a href="userReg.jsp" class="button is-info">
                        Register
                      </a>
                    </span>
                    <span class="nav-item">
                      <a href="SupportLogin.jsp" class="button">
                        Log in
                      </a>
                    </span>
                </div>
            </div>
        </nav>
        </div>
    </div>    
      
  <!-- Hero content: will be in the middle -->
  <div class="hero-body">
    <div class="container has-text-centered">
        <div class="columns is-vcentered">
            <div class="column is-5">
                <figure class="image is-3by4">
                    <img src="images/UEA_LOGO.png" alt="UEA Logo" style="width:600px;height:270px;">
                </figure>
            </div>
        <div class="column is-6 is-offset-1">
      <h1 class="title is-2">
        UEA Reporting Portal  
      </h1>
      <h2 class="subtitle is-4">
        MSc Computing Science Dissertation Project
      </h2>
      <br>
      <p class="has-text-centered">
          <a href="faq.jsp" class="button is-large">
              Learn More
          </a>
      </p>
    </div>
    </div>
    </div>
  </div>

    <!-- Hero footer: will stick at the bottom -->
    <div class="hero-foot">
        <div class="container">
            <div class="tabs is-centered">
                <ul>
                <li class="is-active"><a href="http://www.uea.ac.uk">Image via UEA</a></li>
                <li><a>Project by Caleb Hoffman 2017</a></li>
                </ul>
            </div>
        </div>
    </div>
    </section>
        
      
        

        
        <script async type="text/javascript" src="bulma.js"></script>
    </body>
</html>
