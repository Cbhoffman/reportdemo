<%-- 
    Document   : report
    Created on : 19-Feb-2017, 11:28:43
    Author     : Caleb Hoffman

THIS IS THE OLD REPORT FORM... NO LONGER NEEDED...
--%>

<%@page import="AccessDB.ReportTypes"%>
<%@page import="uk.ac.uea.cmp.javaweb.Reports.Report"%>
<%@page import="java.util.List"%>
<%@page import="uk.ac.uea.cmp.javaweb.Reports"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="reportjs.js" type="text/javascript"></script>
        
        <title>Reporting Home page</title>
    </head>
    <body>
        
        <h1>Report your complaint here.</h1>
            
                <h2>Please select the area that applies the most to your report.</h2>
               <!--
                <input type="radio" name="reportType" value="security">Security<br>
                <input type="radio" name="reportType" value="it">IT<br>
                <input type="radio" name="reportType" value="mtFac">Maintenance and Facilities<br>
                <input type="radio" name="reportType" value="parking">Parking and Transportation
               
                <br />
                <br />
               -->
               <form name= 'report' action='AccessDB' method='post' onSubmit="return validate()" >
                <%
                  List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                  for(ReportTypes.Type type : types)
                  { 
                      String value = type.id;
                      String insert = type.name;
                      
               // out.print(value + " "+insert);
                out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                
                      //type.id;
                      //type.name;
                  }
                %>
                <br />
                <br />
                
                Your Name: <input type="text" name="name" placeholder="Your name" /><br />
                Contact Number: <input type="text" name="contact" /><br />
     <!--       Address: <input type="text" name="address" /><br />
                Postcode: <input type="text" name="postcode" /><br />  -->
                Email: <input type="text" name="email" /><br />
                Report Details: <textarea cols="30" rows="4" name="text"></textarea><br />
                <br />
                
                <input type="submit" value="Submit Your Report"/>
    </form>
                
                
                <h2>Search for additional reports.</h2>
                <form name= 'searchID' action='Search' method='post' >
                Search By Report ID: <input type='text' name='search' placeholder='ID' /><br />
                <br />
                <input type='submit' value='Search' />
                </form>
           
        
        
        

    </body>
</html>
