<%-- 
    Document   : displayReport
    Created on : 01-Mar-2017, 15:37:20
    Author     : Caleb Hoffman

--%>
<!-- NOT NEEDED...

<%@page import="java.util.List"%>
<%@page import="uk.ac.uea.cmp.javaweb.Reports.Report"%>
<%@page import="uk.ac.uea.cmp.javaweb.Reports"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reports Filed</title>
    </head>
    
        <h1>Thank you for reporting.</h1>
        <a href="report.jsp">Report Again</a><br /> <br />
            
        <form name= "clearReports" action="displayReport.jsp" method="post">
               Clear Oldest Report<input type="radio" name="clear" value="one" />
               Clear All Reports<input type="radio" name="clear" value="all" />
                                  <input type="submit" value="Confirm" /> 
               
                                 
                                  
        </form>

        
        <h2>List of Previous Reports:</h2>
       
        
    <%
        Reports c = new Reports();
        
        // Is there a new comment to add at this point? If so add it now.
        // n.b. we could also do this on a seperate page and redirect back
        // such as addComment.jsp but here for simplicity EVEN IF NOT GOOD
        // DESIGN PRACTICE!
        
        String name = request.getParameter("name");
        String text = request.getParameter("text");
        //three below are new
        String contact = request.getParameter("contact");
        String address = request.getParameter("address");
        String postcode = request.getParameter("postcode");
        
        //initial minimal validation
        
      //  if (name != null && text != null)
      //  {
      //      c.Add(name, text, postcode, address, contact);
      //  }
        
        boolean validateOK; //if validateOK == false, do this... etc.
        //redisplay form, add pop-up...
        
        if (name == null || name == "")// below validation does not work properly
        {
            out.print("<p>Error: You must enter a name</p>");
            validateOK = false;
        } else if (contact == "" || contact == null)
        {
            out.print("<p>Error: You must enter a contact number</p>");
            validateOK = false;
        } else if (address == "" || address == null)
        {
            out.print("<p>Error: You must enter an address</p>");
            validateOK = false;
        } else if (postcode == "" || postcode == null)
        {
            out.print("<p>Error: You must enter a postcode</p>");
            validateOK = false;
        } else if (text == "" || text == null)
        {
            out.print("<p>Error: You must enter your report details<p>");
            validateOK = false;
        } else {
            validateOK = true;     
        }
        
        if (validateOK == true)
        {
            c.Add(name, text, postcode, address, contact);
         //   out.print("<p>Report Added!</p>");
        }
        
        // Now read and print the comments
        String deleteStuff =request.getParameter("clear");
        int reportID = 1;
        
        
        if (deleteStuff == null)//since the delete button is not pressed when the page initially loads it will run this
        {
        List<Report> comments = c.Get();
        for(Report comment: comments)          
        {
            out.print(reportID + " <b>By: "+comment.name+", at "+comment.posted+"</b><br /><br />");
            out.print(comment.text+"<br />");
            out.print(comment.contact+"<br />");
            out.print(comment.address+"<br />");
            out.print(comment.postcode+"<br />");
            out.print("<br /><br /><hr />");
            reportID++;
        }// below will run if value is selected from the deletion options
        } 
        else if (deleteStuff == "one"){/// Something wrong here, dont know if the value is being passed from r. button
            List<Report> comments = c.Get();
            out.print(deleteStuff);
            c.RemoveOne();
            for(Report comment: comments)          
            {
                out.print(reportID + " <b>From: "+comment.name+", at "+comment.posted+"</b><br /><br />");
                out.print(comment.text+"<br />");
                out.print(comment.contact+"<br />");
                out.print(comment.address+"<br />");
                out.print(comment.postcode+"<br />");
                out.print("<br /><br /><hr />");
                reportID++;
            }
        }
        else if (deleteStuff == "all"){//should run below if the user selects to delete all reports
            out.print(deleteStuff);
            List<Report> comments = c.Get();
            c.RemoveAll();
            out.print("<b>All reports have been cleared.</b>");
        }
// IF delete button clicked
// THEN run RemoveOne method
        
//Maybe give the button a value of zero, and on click the button will increment in value. 
//Then within the if statement make it progress when the conditional button value is greater than zero.
//Set up the button to run the same jsp action (displayReport.jsp)...

        %>
        
       
        <br />

    
</html>
-->