<%-- 
    Document   : supportReg
    Created on : 14-Jul-2017, 19:06:27
    Author     : Caleb Hoffman
    
NOT NEEDED...
--%>

<%@page import="SystemInteraction.ReportTypes"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>Support Registration</title>
    </head>
    <body>
       <div class="container content is-fluid">  
            
           <div class="columns is-centered"> 
                <div class="column is-two-thirds">
                                  
        <h1>Please Register Here:</h1>
            <form name= 'supportReg' action='SupportReg' method='post'> <!-- onSubmit="return validate()" -->
                <div class="box">
                <label class="label">Which section are you registering for?</label>
                <%
                    List<ReportTypes.Type> types = new ReportTypes().getTypes(); 
                    for(ReportTypes.Type type : types)
                    { 
                        String value = type.id;
                        String insert = type.name;

                        //Displays the different report types (from reporttypes class) within the form.
                        out.println("<input type='radio' name='reportType' value='"+value+"' />"+insert+"<br />");
                    }
                %>   
                <br>  
                <div class="field">
                    <label class="label">First Name</label>
                        <p class="control">
                            <input class="input" type="text" name="fName" placeholder="First Name">
                        </p>
                </div>
                <div class="field">
                    <label class="label">Last Name</label>
                        <p class="control">
                            <input class="input" type="text" name="lName" placeholder="Last Name">
                        </p>
                </div>
                <div class="field">
                    <label class="label">Contact</label>
                        <p class="control">
                            <input class="input" type="text" name="contact" placeholder="Contact">
                        </p>
                </div>

                <div class="field">
                    <label class="label">Email</label>
                    <p class="control">
                      <input class="input" type="text" name="email" placeholder="Email input">
                    </p>      
                </div>
                
                <div class="field">
                    <label class="label">Username</label>
                    <p class="control">
                      <input class="input" type="text" name="username" placeholder="Username">
                    </p>      
                </div>
                
                <div class="field">
                    <label class="label">Password</label>
                    <p class="control">
                      <input class="input" type="password" name="password" placeholder="Password">
                    </p>      
                </div>

                <br />

                <input class="button is-primary" type="submit" value="Complete Registration"/>
                </div>
                
            </form>
        
        <br />        
        
        <a href="home.jsp">Back to Home Page</a>               
                    
                </div>
           </div>  
 
        </div>
        <br>
    </body>
</html>

