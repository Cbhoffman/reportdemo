<%-- 
    Document   : userReg
    Created on : 06-Jul-2017, 17:04:33
    Author     : Caleb Hoffman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="bulma.css">
        <title>New User Registration</title>
    </head>
    <body>
        
       <section class="hero is-fullheight is-light is-bold"> 
           <br>
       <div class="container">         
           <div class="columns is-vcentered"> 
                <div class="column is-4 is-offset-4">
                <h1 class="title">Please Register Here</h1>
            <form name= 'userReg' action='UserReg' method='post'> <!-- onSubmit="return validate()" -->
                <div class="box">
                <div class="field">
                    <label class="label">First Name</label>
                        <p class="control">
                            <input class="input" type="text" name="fName" placeholder="First Name" required>
                        </p>
                </div>
                <div class="field">
                    <label class="label">Last Name</label>
                        <p class="control">
                            <input class="input" type="text" name="lName" placeholder="Last Name" required>
                        </p>
                </div>
                <div class="field">
                    <label class="label">Contact</label>
                        <p class="control">
                            <input class="input" type="text" name="contact" placeholder="Contact" maxlength="11" required>
                        </p>
                </div>


                <div class="field">
                    <label class="label">Email</label>
                    <p class="control">
                      <input class="input" type="text" name="email" placeholder="Email" required>
                    </p>      
                </div>
                
                                <div class="field">
                    <label class="label">Username</label>
                    <p class="control">
                      <input class="input" type="text" name="username" placeholder="Username" required>
                    </p>      
                </div>
                
                <div class="field">
                    <label class="label">Password</label>
                    <p class="control">
                      <input class="input" type="password" name="password" placeholder="Password" required>
                    </p>      
                </div>
                    
                <div class="field">
                    <label class="label">Confirm Password</label>
                    <p class="control">
                      <input class="input" type="password" name="confirmpassword" placeholder="Confirm Password" required>
                    </p>      
                </div>    

             <!--   <p> First Name: <input type="text" name="fName" placeholder="First name" /> </p> 
                <p> Last Name: <input type="text" name="lName" placeholder="Last name" /> </p> 
                <p> Contact Number: <input type="text" name="contact" /> </p>
                <p> Email: <input type="text" name="email" /> </p> -->
                <br />

                <input class="button is-primary" type="submit" value="Complete Registration"/>
                <a class="button is-default" href="home.jsp">Cancel</a>
                </div>
                
            </form>
        
        <br />        
        
        
                    
                    
                </div>
           </div>  
            

           
        </div>
        <br />
       </section>
    </body>
</html>
