/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Caleb Hoffman
 */
public class LoginValidationTest {
    
    public LoginValidationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of validate method, of class LoginValidation.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        String username = "cbhoffman";
        String password = "myz16nqu";
        boolean expResult = false;
        boolean result = LoginValidation.validate(username, password);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of reportTypeForUser method, of class LoginValidation.
     */
    @Test
    public void testReportTypeForUser() {
        System.out.println("reportTypeForUser");
        String username = "cmurphy";
        String expResult = "security";
        String result = LoginValidation.reportTypeForUser(username);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getUserRole method, of class LoginValidation.
     */
    @Test
    public void testGetUserRole() {
        System.out.println("getUserRole");
        String username = "cbhoffman";
        String expResult = "admin";
        String result = LoginValidation.getUserRole(username);
        assertEquals(expResult, result);
    
    }

    /**
     * Test of retrieveStoredPassword method, of class LoginValidation.
     */
    @Test
    public void testRetrieveStoredPassword() {
        System.out.println("retrieveStoredPassword");
        String username = "cbhoffman";
        String expResult = "myz16nqu";
        String result = LoginValidation.retrieveStoredPassword(username);
        assertEquals(expResult, result);
    
    }

    /**
     * Test of getCurrentUser method, of class LoginValidation.
     */
    @Test
    public void testGetCurrentUser() {
        System.out.println("getCurrentUser");
        String username = "cbhoffman";
        int expResult = 1;
        int result = LoginValidation.getCurrentUser(username);
        assertEquals(expResult, result);
    
    }

    /**
     * Test of checkEmailAddress method, of class LoginValidation.
     */
    @Test
    public void testCheckEmailAddress() {
        System.out.println("checkEmailAddress");
        String givenEmail = "chiefscbh@hotmail.com";
        boolean expResult = true;
        boolean result = LoginValidation.checkEmailAddress(givenEmail);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of validatePassword method, of class LoginValidation.
     */
    @Test
    public void testValidatePassword() throws Exception {
        System.out.println("validatePassword");
        String originalPassword = "myz16nqu";
        String storedPassword = "password";
        boolean expResult = false;
        boolean result = LoginValidation.validatePassword(originalPassword, storedPassword);
        assertEquals(expResult, result);
       
    }

  

 
    
}
