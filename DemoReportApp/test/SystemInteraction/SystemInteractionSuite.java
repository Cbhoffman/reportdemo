/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SystemInteraction;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Caleb Hoffman
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({SystemInteraction.AdminViewTest.class, SystemInteraction.UserTest.class, SystemInteraction.LoginValidationTest.class, SystemInteraction.ReportManagerTest.class, SystemInteraction.ReportTypesTest.class, SystemInteraction.RoleChangeSupportTest.class, SystemInteraction.ReportTest.class, SystemInteraction.LogoutTest.class, SystemInteraction.UserManagerTest.class})
public class SystemInteractionSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
}
