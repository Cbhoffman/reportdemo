--// Initial set up of report table //--

CREATE TABLE REPORT 
(ID             SERIAL PRIMARY KEY,
 TIMEOCCURED TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 FNAME           TEXT    NOT NULL,
 LNAME           TEXT    NOT NULL,
 CONTACT        TEXT     NOT NULL,
 EMAIL			TEXT    NOT NULL,
 REPORTTYPE		TEXT    NOT NULL,
 DETAILS        TEXT    NOT NULL,
 IMG			TEXT,
 STATUS         TEXT,
 GEOLOCATION TEXT,
 URGENCY    TEXT);
 
 CREATE TABLE REPORT 
(ID             SERIAL PRIMARY KEY,
 TIMEOCCURED TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 USER_ID	    INT    NOT NULL references userreg(user_id),
 REPORTTYPE		TEXT    NOT NULL,
 DETAILS        TEXT    NOT NULL,
 IMG			TEXT,
 STATUS         TEXT,
 GEOLOCATION TEXT,
 URGENCY    TEXT);
 
CREATE TABLE LOGIN 
(
 user_id         int references userreg(user_id),
 user_login      VARCHAR(40),
 password       VARCHAR,
 reporttype       text);
 
CREATE TABLE USERREG 
(
 user_id 		SERIAL PRIMARY KEY,
 FNAME          TEXT    NOT NULL,
 LNAME          TEXT    NOT NULL,
 CONTACT        TEXT    NOT NULL,
 EMAIL			TEXT    NOT NULL,
 ROLE           TEXT    NOT NULL);
 
CREATE TABLE ADDTAGS (
        ID      SERIAL references REPORT(ID) ON DELETE CASCADE,
		GEOLOCATION TEXT,
		URGENCY    TEXT
);

CREATE TABLE EXTRA (
		EXTRAID	SERIAL,
        ID      SERIAL references REPORT(ID) ON DELETE CASCADE,
		TITLE TEXT,
		INFO    TEXT
);

SELECT id, timeoccured, fname, lname, contact, email, details, img, status, geolocation, urgency
FROM report
LEFT JOIN addtags ON report.ID = addtags.ID
ORDER BY report.id;
        
		
		
 
--// Inserting data into report table //--



--// Types of reports: security, it, mtFac, parking //--

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('tsmith@test.com','door is broken','mtFac', 'images/default.png', 'active', '52.620654, 1.236433', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('bobjones@test.com','There is a streetight thats out in the road','mtFac', 'images/default.png', 'active', '52.622728, 1.242162', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('jsomebody@test.com','Computer is broken in lab','it', 'images/default.png', 'active', '52.622237, 1.240767', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('choff@test.com','Fight in the library','security', 'images/default.png', 'active', '52.620968, 1.240509', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('smiths@test.com','Abandoned car in car park','parking', 'images/default.png', 'active', '52.623180, 1.243176', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('choward@test.com','Broken down car...','parking', 'images/default.png', 'active', '52.622751, 1.240316', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('asmith@test.com','Toilets are clogged in the arts building...','mtFac', 'images/default.png', 'active', '52.622237, 1.240767', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('areid@test.com','Printer is not working...','it', 'images/default.png', 'active', '52.621544, 1.235586', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('jdorsey@test.com','People are starting fires in their dorm rooms...','security', 'images/default.png', 'active', '52.622483, 1.246149', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('tshepard@test.com','Property was stolen in the dorm room','security', 'images/default.png', 'active', '52.622483, 1.246149', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('bwilson@test.com','hooded masked clowns on campus','security', 'images/default.png', 'active', '52.625262, 1.239931', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('jsomeperson@test.com','cars are being broken into','security', 'images/default.png', 'active', '52.623180, 1.243176', 'low');

insert into report (email, details, reporttype, img, status, geolocation, urgency)
values ('tjohnson@test.com','door is broken','mtFac', 'images/default.png', 'active', '52.620472, 1.234386', 'low');


--//INSERT USERS//--

insert into userreg (fname, lname, contact, email)
values ('Ted','Smith', '01234567891', 'tsmith@test.com');

insert into userreg (fname, lname, contact, email)
values ('Bob', 'Jones', '01234567891', 'bobjones@test.com');

insert into userreg (fname, lname, contact, email)
values ('Joe', 'Somebody', '07886756435', 'jsomebody@test.com');

insert into userreg (fname, lname, contact, email)
values ('CB', 'Hoffman', '07987685463', 'choff@test.com');

insert into userreg (fname, lname, contact, email)
values ('Sean', 'Smith', '09878967546', 'smiths@test.com');

insert into userreg (fname, lname, contact, email)
values ('Charles', 'Howard', '09826345744', 'choward@test.com');

insert into userreg (fname, lname, contact, email)
values ('Alex', 'Smith', '07867554673', 'asmith@test.com');

insert into userreg (fname, lname, contact, email)
values ('Andy', 'Reid', '04534356745', 'areid@test.com');

insert into userreg (fname, lname, contact, email)
values ('John', 'Dorsey', '05645354678', 'jdorsey@test.com');

insert into userreg (fname, lname, contact, email)
values ('Ty', 'Shepard', '07756476879', 'tshepard@test.com');

insert into userreg (fname, lname, contact, email)
values ('Brian', 'Wilson', '08976543212', 'bwilson@test.com');

insert into userreg (fname, lname, contact, email)
values ('Joanne', 'Someperson', '08976565764', 'jsomeperson@test.com');

insert into userreg (fname, lname, contact, email)
values ('Ted', 'Johnson', '01234567891', 'tjohnson@test.com');

--//Inserting usernames and passwords//--

insert into supportreg (user_login, password, reporttype)
values ('securitysupport', 'security', 'security');

insert into supportreg (user_login, password, reporttype)
values ('itsupport', 'itsupport', 'it');

insert into supportreg (user_login, password, reporttype)
values ('mtfacsupport', 'mtfac', 'mtfac');

insert into supportreg (user_login, password, reporttype)
values ('parkingsupport', 'parking', 'parking');

insert into supportreg (user_login, password, reporttype)
values ('sunionsupport', 'sunion', 'union');

--//Inserting usernames and passwords//--

insert into login (user_id, user_login, password, reporttype)
values (1 ,'securitysupport', 'security', 'security');

insert into login (user_id, user_login, password, reporttype)
values (2, 'itsupport', 'itsupport', 'it');

insert into login (user_id, user_login, password, reporttype)
values (3, 'mtfacsupport', 'mtfac', 'mtfac');

insert into login (user_id, user_login, password, reporttype)
values (4, 'parkingsupport', 'parking', 'parking');

insert into login (user_id, user_login, password, reporttype)
values (5, 'sunionsupport', 'sunion', 'union');


--//Search query//--

select * from report where name = 'Bob';

--//Update status of report//--

UPDATE report
SET status = 'resolved'
WHERE id = ?;

--//Insert into User Table//--

insert into userreg (fname, lname, contact, email)
values ('Bob','Smith', '07867565434', 'bsmith@test.com');


--//Coordinates around UEA campus//--

Enterprise Centre- '52.625262, 1.239931'
Sportspark- '52.624271, 1.241059'
Main Visitors Car Park- '52.623180, 1.243176'
Arts '2' Building- '52.622237, 1.240767'
CMP Building- '52.620654, 1.236433'
Sainsburys Centre- '52.620472, 1.234386'
EFry- '52.621544, 1.235586'
Thomas Paine Study Centre- '52.621870, 1.235135'
Julian Study Centre- '52.622057, 1.234079'
Lawrence Steinhouse Building- '52.621604, 1.237013'
Library- '52.620968, 1.240509'
Student Unio- '52.621532, 1.241452'
Chancellors Drive- '52.622751, 1.240316'
University Drive- '52.622728, 1.242162'
Dorm- '52.622483, 1.246149'
			
			